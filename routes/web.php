<?php

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('confirmation/{token}', 'Auth\RegisterController@confirmation')
    ->name('confirmation');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');;
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Social authentication
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider')->name('provider');
Route::get('{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

Route::get('/', function () {
    return view('welcome');
});

Route::resource('audits', 'AuditController');

Route::get('/chat', function () {
    return view('chat');
})->middleware("auth");

Route::get('/messages', function () {
    return App\Message::with('user')->get();
})->middleware('auth');

Route::post('/messages', function () {
    $user = Auth::user();
    $message = $user->messages()->create([
      "message" => request()->get('message')
    ]);

    broadcast(new MessagePosted($message, $user))->toOthers();

    return ['status' => 'ok'];
})->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('/file', function(Request $request){
  if ($request->hasFile('file')) {
    $file = $request->file('file');
    $ext  = $file->guessClientExtension();

    $path = $file->storeAs('files/', 'avatar.'.$ext);
    // $path = $request->file('file')->store('files');

    return $path;
  }else{
    return "no file";
  }
});

Route::group(['namespace' => 'Backend', 'middleware' => 'auth'], function (){
    /* Rutas para controlador AccountsController */
    Route::get('my-account', 'AccountsController@getIndex')->name('account');
    Route::post('update-account', 'AccountsController@postUpdate')->name('update_account');
    Route::post('drop-account', 'AccountsController@postDropOut')->name('drop_account');
});

// Usuario - Perfil
Route::resource('user', 'UsersController');

// Employee's routes
Route::resource('employees', 'EmployeesController');
Route::get('employees/{id}/destroy',[
    'uses' => 'EmployeesController@destroy',
    'as' => 'employees.destroy'
]);

Route::resource('employees-project', 'EmployeesProjectController');


Route::resource('employees-department', 'EmployeesDepartmentController');

// Profile picture route
Route::get('storage/app/public/profile_pictures/{folder}/{image}', function($folder = null, $image = null) {
	$path = storage_path().'/app/public/profile_pictures/'.$folder.'/'.$image;
	return Response::download($path);
})->name('image');

/* Ruta de departamentos*/
Route::resource('department', 'DepartmentsController');
    Route::get('department/{id}/destroy',[
          'uses' => 'DepartmentsController@destroy', 
          'as' => 'department.destroy'
        ]);

/* Ruta de cargos */
Route::resource('charge', 'ChargesController');
Route::get('charge/{id}/destroy',[
  'uses' => 'ChargesController@destroy',
  'as' => 'charge.destroy'
]);

/* Ruta de Proyectos */
Route::resource('project', 'ProjectsController');
Route::get('project/{id}/destroy',[
    'uses' => 'ProjectsController@destroy',
    'as' => 'project.destroy'
]);
// GENERAR PDF - ILDEMAR
Route::get('project/{id}/generatePDF',[
    'uses' => 'ProjectsController@generatePDF',
    'as' => 'project.pdf'
]);

/* Ruta de Tareas */
Route::resource('task', 'TasksController');
Route::get('task/{id}/destroy',[
    'uses' => 'TasksController@destroy',
    'as' => 'task.destroy'
]);

/* Ruta de Documents */

Route::resource('documents', 'DocumentsController');

// PDF
Route::get("/documents/pdf/constancia", function(){

  $pdf = PDF::loadView('documents.pdf');
  return $pdf->stream();

});

Route::post('/file', function(Request $request){
  if ($request->hasFile('file')) {
    $file = $request->file('file');
    $ext  = $file->guessClientExtension();

    $path = $file->storeAs('files/', 'avatar.'.$ext);
    // $path = $request->file('file')->store('files');

    return $path;
  }else{
    return "no file";
  }
});
