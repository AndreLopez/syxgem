<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description', 
        'analysis',
        'picture'
        
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', '_method'];

    /**
     * Get employees of a department
     *
     * @return array
     */
    public static function total_employees()
    {
        $arr = [];
        foreach (Department::all() as $item)
        {
            $val = Employee::where('department_id', $item->id)->count();
            array_push($arr, $val);
        }
        return $arr;
    }
    public function employee()
    {
        return $this->hasMany('App\Models\Employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function audits()
    {
        return $this->hasMany('App\Models\Audit');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany('App\Models\Project');
    }

}
