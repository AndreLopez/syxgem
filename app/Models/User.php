<?php

namespace App\Models;

use App\Models\SocialProvider;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Boot the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function ($user) {
            $user->confirmation_token = hash_hmac('sha256', str_random(40), $user->email);
        });
    }

    /**
     * Confirm email, set confirmation_token in null
     *
     * @return void
     */
    public function confirmEmail()
    {
        $this->confirmation_token = null;
        $this->save();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function socialProviders()
    {
        return $this->hasMany(SocialProvider::class);
    }

    public function setPasswordAttribute($value)
    {
        if(!empty($value)){
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Employee');
    }
}
