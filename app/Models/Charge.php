<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Charge extends Model
{

 use SoftDeletes;
    

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
    	'description', 
    	'analysis'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    //Relaciones 


    public function Departments()
    {
        return $this->belongsToMany('App\Model\Department');
    } 




}
