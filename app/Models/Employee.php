<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'first_name',
        'last_name',
        'email',
        'tasks',
        'skills',
        'hire_date',
        'phone',
        'identification',
        'profile_picture',
        'department_id',
        'charge_id',
        'status_id',
        'users_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }


    public function charge()
    {
        return $this->belongsTo('App\Models\Charge');
    }

    public function project()
    {
        return $this->belongsToMany('App\Models\Project');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany('App\Models\Task');
    }

    public function user()
    {
        return $this->belongsToMany('App\Models\User');
    }

}