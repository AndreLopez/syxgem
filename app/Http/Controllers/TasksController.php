<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // todas las tareas
        $tasks = Task::paginate();
        return view('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // todos los proyectos
        $projects  = Project::all();
        // todos los empleados
        $employees = Employee::all();

        return view('tasks.create', compact('projects', 'employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation
        $this->validate($request, [
            'name'        => 'required',
            'description' => 'required',
            'project_id'  => 'required',
            'employee_id' => 'required',
        ]);

        $task = new Task();
        $task->fill($request->all());

        // guardar
        if ($task->save())
        {
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.created')
            ]);

            return redirect()->route('task.index');
        }
        else
        {
            // todos los proyectos
            $projects  = Project::all();
            // todos los empleados
            $employees = Employee::all();

            return view('tasks.create', compact('projects', 'employees'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // información de una tarea
        $task = Task::find($id);

        return view('tasks.view', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // información de una tarea
        $task = Task::find($id);

        // proyectos
        $projects = Project::all();

        // empleados
        $employees = Employee::all();

        return view('tasks.edit', compact('task', 'projects', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation
        $this->validate($request, [
            'name'        => 'required',
            'description' => 'required',
            'project_id'  => 'required',
            'employee_id' => 'required',
        ]);

        $task = Task::find($id);
        $task->fill($request->all());

        // guardar
        if ($task->save())
        {
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.created')
            ]);

            return redirect()->route('task.index');
        }
        else
        {
            // todos los proyectos
            $projects  = Project::all();
            // todos los empleados
            $employees = Employee::all();

            return view('tasks.edit', compact('projects', 'employees'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Task::destroy($id);
        return redirect('task');
    }
}
