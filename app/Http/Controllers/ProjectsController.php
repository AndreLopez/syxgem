<?php


namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Task;
use App\Models\ProjectTask;
use App\Http\Requests\StatusRequest;
use App\Models\ProjectEmployee;
use Barryvdh\DomPDF\Facade as PDF;


class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::paginate();
        return view('project.index', compact('projects'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        $employees   = Employee::all();

        return view('project.create', compact('departments', 'employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation
        $this->validate($request, [
            'name'           => 'required',
            'initiated_date' => 'required',
            'finished_date'  => 'required',
            'client_name'    => 'required',
            'estimated_time' => 'required',
        ]);

        $projects = new Project();
        $projects->fill($request->all());
        $projects->code = $this->generateCode();


        if ($projects->save())
        {
            foreach ($request->employee_id as $item)
            {
                ProjectEmployee::create([
                    'project_id' => $projects->id,
                    'employee_id' => $item,
                ]);
            }
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.created')
            ]);

            return redirect()->route('project.index');
        }
        else
        {
            $departments = Department::all();
            $employees   = Employee::all();

            return view('project.create', compact('departments', 'employees'));
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project          = Project::find($id); // proyecto
        $tasks            = Task::where('project_id', $id)->get();
        $employees        = Employee::all(); // todos los empleados
        $project_employee = ProjectEmployee::where('project_id', $id)->get(); // empleados de un proyecto

        return view('project.view', compact('project', 'departments', 'employees', 'project_employee', 'tasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project          = Project::find($id); // proyecto
        $departments      = Department::all(); // todos los departamentos
        $employees        = Employee::all(); // todos los empleados
        $project_employee = ProjectEmployee::where('project_id', $id)->get(); // empleados de un proyecto

        return view('project.edit', compact('project', 'departments', 'employees', 'project_employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation
        $this->validate($request, [
            'name'           => 'required',
            'initiated_date' => 'required',
            'finished_date'  => 'required',
            'client_name'    => 'required',
            'estimated_time' => 'required',
            'wiki'           => 'required'
        ]);

        // get project
        $project = Project::find($id);
        $project->fill($request->all());

        // save values
        if ($project->save())
        {
            foreach ($request->employee_id as $item)
            {
                ProjectEmployee::where('project_id', $id)->update([
                    'project_id' => $project->id,
                    'employee_id' => $item
                ]);
            }

            $request->session()->flash('message', [
                'alert' => 'success',
                'text'  => trans('dashboard.messages.created')
            ]);

            return redirect()->route('project.index');
        }
        else
        {
            $request->session()->flash('message', [
                'alert' => 'error',
                'text'  => trans('dashboard.messages.created')
            ]);

            return view('project.edit', compact('project'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::destroy($id);

        return redirect('project');
    }

    /**
     * Generar PDF
     *
     * @param $id
     * @return mixed PDF
     */
    public function generatePDF($id)
    {
        $project          = Project::find($id); // proyecto
        $employees        = Employee::all(); // todos los empleados
        $project_employee = ProjectEmployee::where('project_id', $id)->get(); // empleados de un proyecto

        // cargar la vista con la info
        $pdf = PDF::loadView('project.PDF', compact('project', 'departments', 'employees', 'project_employee'));

        // descargar el pdf
        return $pdf->download('Proyecto-'.$project->code.'.pdf');
    }

    /**
     * Generar código random
     *
     * @return string codigo
     */
    private function generateCode()
    {
        $code = '';
        $pattern = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
        $max = strlen($pattern)-1;

        for($i = 0; $i < 8; $i++)
        {
            $code .= $pattern{mt_rand(0, $max)};
        }
        return $code;
    }
}
