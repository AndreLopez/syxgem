<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Department;
use App\Models\Charge;

class EmployeesProjectController extends Controller
{
    public function index()
    {
        $employees = Employee::paginate();
        return view('employees.for-project', ['employees' => $employees]);
    }
}
