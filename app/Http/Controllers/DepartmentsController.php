<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Http\Requests\StatusRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request as RequestObj;
Use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments     = Department::paginate();
        $total_employees = Department::total_employees();

        return view('department.index', compact('departments', 'total_employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('department.create');  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $departments = new Department();

        // fill from inputs depends if the request have a file
        if ($request->hasFile('picture'))
        {
            $departments->fill($request->all());
            $departments->picture = $request->name.'.jpg';
            // Upload Profile Picture
            $request->file('picture')->storeAs(
                'public/departmentPictures/'.$departments->name, $departments->picture);
        }
        else
        {
            $departments->fill($request->all());
        }
        // save process
        if ($departments->save())
        {
            // message
            $request->session()->flash('message', [
                'alert' => 'success',
                'text'  => trans('dashboard.messages.updated')
            ]);
            // redirect
            return redirect()->route('department.index');
        }
        else
        {
            // message
            $request->session()->flash('message', [
                'alert' => 'error',
                'text'  => trans('dashboard.messages.updated')
            ]);
            // redirect
            return view('department.create', compact('departments'));
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);

        return view('department.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // get department
        $departments = Department::findOrFail($id);

        if (!is_null($departments))
        {
            // fill from inputs depends if the request have a file
            if ($request->hasFile('picture'))
            {
                $departments->fill($request->all());
                $departments->picture = $request->name.'.jpg';
                // Upload Profile Picture
                $request->file('picture')->storeAs(
                    'public/departmentPictures/'.$departments->name, $departments->picture);
            }
            else
            {
                $departments->fill($request->all());
            }

            // save process
            if ($departments->save())
            {
                // message
                $request->session()->flash('message', [
                    'alert' => 'success',
                    'text'  => trans('dashboard.messages.updated')
                ]);
                // redirect
                return redirect()->route('department.index');
            }
            else
            {
                // message
                $request->session()->flash('message', [
                    'alert' => 'error',
                    'text'  => trans('dashboard.messages.updated')
                ]);
                // redirect
                return view('department.edit', compact('departments'));
            }
        }
        return redirect()->route('department.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departments = Department::findOrFail($id); 
        $departments->delete();

        return redirect()->route('department.index');
    }
}
