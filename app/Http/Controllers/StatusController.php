<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Statu;
use App\Http\Requests\StatusRequest;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = \App\Models\Statu::paginate();
        return view('status.index', compact('status'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = new  Statu();
        $status->name = $request->get('name');
        $status->save();

        
        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('status.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Statu::find($id); 
        return view('status.edit', compact('status')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $status = Statu::find($id);


        $status->name  = $request->name;
        $status->save();

        
        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('status.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     
      dd('Hola'); 

     //if($request->ajax()){            
            //$status = \App\Models\Statu::find($id);
            //$status->delete();
            //$status_total = \App\Models\Statu::all()->count();
            
            //return response()->json([
             //   'total' => $status_total,
             //   'message' =>  $status->name . ' fue eliminado correctamente'
           // ]);
           
       // }
    }
 }


