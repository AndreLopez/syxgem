<?php

namespace App\Http\Controllers;

use App\Models\Audit;
use App\Models\Department;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $audits = Audit::all();
      return view("audits.index",["audits" => $audits]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // todos los departamentos
        $departaments = Department::all();

        // todos los proyectos
        $projects = Project::all();

      return view("audits.create", compact('departaments', 'projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Validation
      $this->validate($request, [
          'audit_name'     => 'required',
          'comments'         => 'required'/*,
          'reports_id'        => 'required',
          'proyects_id' => 'required',
          'departaments_id'  => 'required'*/
      ]);

      $audit = new Audit;

      $audit->audit_name        = $request->input("audit_name");
      $audit->comments          = $request->input("comments");
      $audit->reports_id        = $request->input("reports_id");
      $audit->proyects_id       = $request->input("proyects_id");
      $audit->departaments_id   = $request->input("departaments_id");

      if ($request->hasFile('file')) {
        // $file = $request->file('file');
        // $ext  = $file->guessClientExtension();
        /*$path = $file->storeAs('files/', 'avatar.'.$ext);*/

        $path = $request->file('file')->store('files');

        $audit->pathfile = $path;
        // return $path;

      }else{
        $audit->pathfile = null;
      }
      // return "nada";

      if($audit->save()){
        $request->session()->flash('flash_message', 'Success!');
        return redirect('audits');
      }else{
        return view('audits.create');
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Audit $audit)
    {
      return view("reports.show",["audit" => $audit]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Audit $audit)
    {
      return view('report.edit', ['audit' => $audit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Audit $audit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Audit $audit)
    {
        $audit->delete();
    }
    
}
