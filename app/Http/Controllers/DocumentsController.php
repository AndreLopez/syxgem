<?php

namespace App\Http\Controllers;

use App\Models\Audit;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $documents = Document::all();
      return view("documents.index", ["documents" => $documents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $audits = Audit::all();

        return view("documents.create", compact('audits'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Validation
      $this->validate($request, [
          'code'              => 'required|unique:documents',
          'name'              => 'required',
          'documents_details' => 'required',
          'comments'          => 'required'
      ]);

      $document = new Document;

      $document->name               = $request->input("name");
      $document->code               = $request->input("code");
      $document->comments           = $request->input("comments");
      $document->documents_details  = $request->input("documents_details");
      $document->reports_id         = $request->input("reports_id");
      $document->audits_id          = $request->input("audits_id");
      $document->restricted         = (bool) $request->input("restricted");

      if ($request->hasFile('file')) {

        $path = $request->file('file')->store('files');

        $document->drive = $path;

      }else{
        $document->drive = null;
      }

      if($document->save()){
        $request->session()->flash('flash_message', 'Success!');
        return redirect('documents');
      }else{
        return view('documents.create');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $document = Document::find($id);
      $audits = Audit::all();

      return view("documents.show",["document" => $document]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $document = Document::find($id);
      $audits = Audit::all();

      if($document->restricted) {
        session()->flash('flash_alert', 'Este documento esta restringido para modificación');
        return redirect('documents');
      }
      else return view("documents.edit",["document" => $document, 'audits' => $audits]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $document = Document::find($id);

      $document->name               = $request->input("name");
      $document->code               = $request->input("code");
      $document->comments           = $request->input("comments");
      $document->documents_details  = $request->input("documents_details");
      $document->reports_id         = $request->input("reports_id");
      $document->audits_id          = $request->input("audits_id");
      $document->restricted         = (bool) $request->input("restricted");

      if ($request->hasFile('file')) {

        $path = $request->file('file')->store('files');

        $document->drive = $path;

      }else{
        $document->drive = null;
      }

      if($document->save()){
        $request->session()->flash('flash_message', 'Edit Success!');
        return redirect('documents');
      }else{
        return view('documents.create');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = Document::find($id);
        $document->delete();

        if ($document->trashed()) {
          session()->flash('flash_message', 'Delete Success!');
          return redirect('documents');
        }else{
          session()->flash('flash_alert', 'Error Deleting');
          return redirect('documents');
        }
    }
}
