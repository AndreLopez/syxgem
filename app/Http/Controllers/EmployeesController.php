<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Department;
use App\Models\Charge;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::paginate();
        return view('employees.index', ['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        $charges = Charge::all();
        return view('employees.create', compact('departments', 'charges'));
    }

    public function project()
    {
        $employees = Employee::paginate();
        return view('employees.index', ['employees' => $employees]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation
        $this->validate($request, [
            'first_name'     => 'required',
            'last_name'      => 'required',
            'email'          => 'required',
            'tasks'          => 'required',
            'skills'         => 'required',
            'hire_date'      => 'required',
            'phone'          => 'required',
            'identification' => 'required',
            'department_id'  => 'required',
            'charge_id'      => 'required'
        ]);
        // Input values
        $employee = new Employee;


        // If input file has file, upload it
        if ($request->hasFile('profile_picture'))
        {
            $employee->fill($request->all());
            $employee->code            = $this->codeGenerator($request->email);
            $employee->profile_picture = $request->first_name.'_'.$request->last_name.'.jpg';
            // Upload Profile Picture
            $request->file('profile_picture')->storeAs('public/profile_pictures/'.$employee->code, $employee->profile_picture);
        }
        else
        {
            $employee->code = $this->codeGenerator($request->email);
            $employee->fill($request->all());
        }

        if ($employee->save())
        {
            $request->session()->flash('flash_message', 'Success!');
            return redirect('employees');
        }
        else
        {
            return view('employees.create', compact('employee'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        $tasks = Employee::find($id)->tasks()->get();

        //dd($tasks);

        return view('employees.view', compact('employee', 'tasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee    = Employee::find($id);
        $departments = Department::all();
        $charges     = Charge::all();

        return view('employees.edit', compact('employee', 'departments', 'charges'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validations
        $this->validate($request, [
            'first_name'     => 'required',
            'last_name'      => 'required',
            'email'          => 'required',
            'tasks'          => 'required',
            'skills'         => 'required',
            'hire_date'      => 'required',
            'phone'          => 'required',
            'identification' => 'required'
        ]);

        $employee = Employee::find($id);

        // If input file has file, upload it
        if ($request->hasFile('profile_picture'))
        {
            $employee->fill($request->all());
            $employee->profile_picture = $request->first_name.'_'.$request->last_name.'.jpg';
            // upload picture
            $request->file('profile_picture')->storeAs('public/profile_pictures/'.$employee->code, $employee->profile_picture);
        }
        else
        {
            $employee->fill($request->all());
        }

        if ($employee->save())
        {
            $request->session()->flash('flash_message', 'Success!');
            return redirect('employees');
        }
        else
        {
            return view('employees.edit', ['employee' => $employee]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();

        return redirect('employees');
    }

    /**
     * Generate random code from user email
     *
     * @param  string $email
     * @return string User code
     */
    private function codeGenerator($email)
    {
        $key = '';
        $length = 5;
        $pattern = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
        $max = strlen($pattern)-1;

        for($i = 0; $i < $length; $i++)
        {
            $key .= $pattern{mt_rand(0, $max)};
        }
        return substr($email, 0, 4).'-'.$key;
    }
}
