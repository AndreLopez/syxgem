<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class AccountsController extends Controller
{
    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function getIndex()
    {
        return view('accounts.index');
    }

    public function postUpdate(Request $request)
    {
        $user = Auth::user();

        $passwdRules = ($request->has('password')) ? 'min:6|confirmed' : '';

        \Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => ['required','email','max:255',Rule::unique('users', 'email')->ignore($user->id)],
            'password' => $passwdRules,
        ])->validate();

        if ($request->has('password')){
            $user->fill($request->all());
        } else {
            $user->fill($request->except('password'));
        }
        $user->save();
        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('messages.updated')
        ]);
        return redirect()->route('account');
    }

    public function postDropOut()
    {
        Auth::user()->delete();
        return redirect('/');
    }
}
