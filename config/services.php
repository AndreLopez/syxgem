<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1451875675111911',
        'client_secret' => '07bd85be38ac7a58bfa99c9d6ead5884',
        'redirect' => 'http://sysgem.app/facebook/callback',
    ],

    'twitter' => [
        'client_id' => 'ut7NQQmtjBd8z8vRMb3dztEk0',
        'client_secret' => 'vRFi3UbHdM3IVXA38GYpvlAm0g7XNGCNhIWc2k6XWpItXi3wpJ',
        'redirect' => 'http://sysgem.app/twitter/callback',
    ],

    'google' => [
        'client_id' => '258809117424-jn9sbjvds2k111tldfa37l30koevt69a.apps.googleusercontent.com',
        'client_secret' => 'Ff5k9DlD8HRAu9CDozUWJHyh',
        'redirect' => 'http://sysgem.app/google/callback',
    ],
];
