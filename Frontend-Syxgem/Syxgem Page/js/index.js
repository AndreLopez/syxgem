var vid = document.getElementById("bgvid");
var pauseButton = document.querySelector("#polina button");
var text_video_1 = document.getElementById("text-1");
var text_video_2 = document.getElementById("text-2");


if (window.matchMedia('(prefers-reduced-motion)').matches) {
    vid.removeAttribute("autoplay");
    vid.pause();
    pauseButton.innerHTML = "Continue";
}

function vidFade() {
  vid.classList.add("stopfade");
}

vid.addEventListener('ended', function()
{
// only functional if "loop" is removed 
vid.pause();
// to capture IE10
vidFade();
}); 


pauseButton.addEventListener("click", function() {
  if (vid.paused) {
    vid.play();
    pauseButton.innerHTML = "Pause";
    text_video_1.innerHTML = " ";
    text_video_2.innerHTML = " ";
     vid.classList.remove("stopfade"); 
  } else {
    vid.pause();
    pauseButton.innerHTML = "Continue";
     vid.classList.toggle("stopfade");    
  }
})