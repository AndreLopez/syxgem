<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id', 10);
            $table->string('code', 8)->unique();
            $table->string('name', 75)->nullable();
            $table->date('initiated_date')->nullable();
            $table->date('finished_date')->nullable();
            $table->string('client_name', 100)->nullable();
            $table->string('tasks', 100)->nullable();
            $table->text('details_tasks')->nullable();
            $table->string('sub_tasks', 100)->nullable();
            $table->string('estimated_time', 100)->nullable();
            $table->text('goals')->nullable();
            $table->text('wiki')->nullable();
            $table->text('comments')->nullable();
            $table->integer('department_id')->unsigned();

            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');



            $table->timestamps();
            $table->softDeletes()->comment('timestamp in that record has deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
