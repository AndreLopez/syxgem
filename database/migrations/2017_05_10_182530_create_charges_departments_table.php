<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargesDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charges_departments', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('charges_id')->unsigned();
            $table->foreign('charges_id')->references('id')->on('charges');

            
            $table->integer('departments_id')->unsigned();
            $table->foreign('departments_id')->references('id')->on('departments');
  

            $table->softDeletes(); 
            $table->timestamps();
            $table->rememberToken();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charges_departments');
    }
}
