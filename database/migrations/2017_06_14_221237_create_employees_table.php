<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table){
            $table->increments('id', 10);
            $table->string('code', 10)->unique();
            $table->string('first_name', 75);
            $table->string('last_name', 75);
            $table->string('email', 90)->unique();
            $table->text('tasks');
            $table->text('skills');
            $table->datetime('hire_date');
            $table->string('phone', 45);
            $table->string('identification', 45);
            $table->longText('profile_picture')->nullable();
            
            $table->integer('department_id')->unsigned();
            $table->integer('charge_id')->unsigned();
             
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade'); 
            $table->foreign('charge_id')->references('id')->on('charges')->onDelete('cascade'); 


            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}