<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Administrador',
                'email' => 'admin@admin.com',
                'password' => '$2y$10$bDjhatsNVbrphOa7kJA1VOyan2KQh7wrBckJL3qSxBYkwwFEyQ3XS', // 123456
                'active' => 1,
                'confirmation_token' => null,
                'remember_token' => 'Lqo4MNkKIs71Lt4gFPhb5zrPZkCS3C07VsRQiEIsNnyMPDjVrZtCaQBeKW4c',
                'created_at' => '2017-05-26 17:30:38',
                'updated_at' => '2017-05-26 17:30:38'
            ],
            [
                'name' => 'Usuario',
                'email' => 'user@user.com',
                'password' => '$2y$10$bDjhatsNVbrphOa7kJA1VOyan2KQh7wrBckJL3qSxBYkwwFEyQ3XS', // 123456
                'active' => 1,
                'confirmation_token' => null,
                'remember_token' => 'dSgljpmThDqI3RLyoFMblwp5eRZVrpiARiWKXWte1tSrl88h4kEeLjubD5aX',
                'created_at' => '2017-05-26 17:30:38',
                'updated_at' => '2017-05-26 17:30:38'
            ]
        ]);
    }
}
