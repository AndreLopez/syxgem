# Syxgem 

Sistema para la gestión de los proyectos  y sus actividades en una empresa  u organismo, a fin de mantener una mejor interacción con los empleados encargados del área asignada; así mismo visualizar la ejecución de las actividades y del equipo quien labora en su construcción.    

## Requerimientos
Los requerimientos mínimos para el correcto funcionamiento de la aplicación son:

>>>
*	PHP  5.6.4 o 5.7
*	OpenSSL PHP Extension
*	PDO PHP Extension
*	Mbstring PHP Extension
*	Tokenizer PHP Extension
*	XML PHP Extension
*	Composer
*	Laravel   5.4
*	JavaScript , Ajax, , jQuery. 
*	Node.js
*	Vue.js
>>>

## Instalación
#### Para la gestión de dependencias, la aplicación utiliza composer el cual debe estar instalado en el servidor en el que se coloque la aplicación. 

#### Una vez instalado y configurado el composer, se debe descargar el proyecto del repositorio de la siguiente forma:


>>> git clone https://gitlab.com/proyectosismcenter/sysgem.git



#### Esto descargara la estructura base de la aplicación.Posteriormente se debe actualizar las dependencias de la herramienta para su correcto funcionamiento, para ello se debe ejecutar: 


>>> composer update 

#### Esto desargará todas las dependencias requeridas por la aplicación y generará todos los archivos de configuración correspondientes.

#### El siguiente paso es crear el archivo .env que contendrá la configuración de acceso a la aplicación, base de datos, correo electrónico, etc. Para esto copiamos el archivo .env.example con el nombre .env y se modifican los valores de las variables de configuración de acuerdo al entorno de instalación en donde hayamos instalado la aplicación. 

#### Por defecto, cuando se descarga e instalan las dependencias del sistema, la clave única del sistema se encuentra vacía por lo que es necesario generarla con el siguiente comando: 


>>> php artisan key:generate 


#### El siguiente paso a realizar es la migración de la estructura de la base de datos, para lo cual se debe haber configurado correctamente el archivo .env (como se mencionó con anterioridad) 
#### La migración de la estructura de la base de datos se realiza con el comando


>>> php artisan migrate 


#### y los datos básicos iniciales se registran con el comando


>>> php artisan db:seed --class: NOMBRE_CLASE 

#### Donde NOMBRE_CLASE  es el nombre de la clase que contiene las instrucciones para el registro de información inicial en la aplicación, estas se encuentran dentro de la carpeta database/seeds.

## Recordar:

#### Llevar un orden con los modelos, controladores y vistas. Cada archivo debe ir en la carpeta correspondiente,  para  que de esta manera haya una mayor organización del proyecto y del contenido del mismo. 

Sistema para la gestión de los proyectos  y sus actividades en una empresa  u organismo, a fin de mantener una mejor interacción con los empleados encargados del área asignada; así mismo visualizar la ejecución de las actividades y del equipo quien labora en su construcción.    

## Requerimientos
Los requerimientos mínimos para el correcto funcionamiento de la aplicación son:

>>>
*	PHP  5.6.4 o 5.7
*	OpenSSL PHP Extension
*	PDO PHP Extension
*	Mbstring PHP Extension
*	Tokenizer PHP Extension
*	XML PHP Extension
*	Composer
*	Laravel   5.4
*	JavaScript , Ajax, , jQuery. 
*	Node.js
*	Vue.js
>>>

## Instalación
#### Para la gestión de dependencias, la aplicación utiliza composer el cual debe estar instalado en el servidor en el que se coloque la aplicación. 

#### Una vez instalado y configurado el composer, se debe descargar el proyecto del repositorio de la siguiente forma:


>>> git clone https://gitlab.com/proyectosismcenter/sysgem.git



#### Esto descargara la estructura base de la aplicación.Posteriormente se debe actualizar las dependencias de la herramienta para su correcto funcionamiento, para ello se debe ejecutar: 


>>> composer update 

#### Esto desargará todas las dependencias requeridas por la aplicación y generará todos los archivos de configuración correspondientes.

#### El siguiente paso es crear el archivo .env que contendrá la configuración de acceso a la aplicación, base de datos, correo electrónico, etc. Para esto copiamos el archivo .env.example con el nombre .env y se modifican los valores de las variables de configuración de acuerdo al entorno de instalación en donde hayamos instalado la aplicación. 

#### Por defecto, cuando se descarga e instalan las dependencias del sistema, la clave única del sistema se encuentra vacía por lo que es necesario generarla con el siguiente comando: 


>>> php artisan key:generate 


#### El siguiente paso a realizar es la migración de la estructura de la base de datos, para lo cual se debe haber configurado correctamente el archivo .env (como se mencionó con anterioridad) 
#### La migración de la estructura de la base de datos se realiza con el comando


>>> php artisan migrate 


#### y los datos básicos iniciales se registran con el comando


>>> php artisan db:seed --class: NOMBRE_CLASE 

#### Donde NOMBRE_CLASE  es el nombre de la clase que contiene las instrucciones para el registro de información inicial en la aplicación, estas se encuentran dentro de la carpeta database/seeds.

## Recordar:

#### Llevar un orden con los modelos, controladores y vistas. Cada archivo debe ir en la carpeta correspondiente,  para  que de esta manera haya una mayor organización del proyecto y del contenido del mismo. 

# Sysgem 