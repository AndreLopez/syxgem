@extends('layouts.app')
@section('content')

    <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45">Por Proyecto</h1>
    </div><!-- /.page-header -->

    <div class="row">

        <div class="col-xs-12 bord-table">
            <div class="table-responsive">

                <table class="table table-hover">
                    <thead class="text-primary doc">
                    <tr>
                        <th class="first-th">Nombre</th>
                        <th>Miembros</th>
                        <th>Apellido</th>
                        <th>Correo</th>
                        <th>Departamento</th>
                        <th class="ultim-th">Acción</th>
                    </tr>
                    </thead>
                    <tbody align="center">
                    @foreach (\App\Models\Employee::all() as $employee)
                        <tr class="tr">
                            <td><a href="{{ url('employees/'.$employee->id) }}">{{ $employee->code }}</a></td>
                            <td>{{ $employee->first_name }}</td>
                            <td>{{ $employee->last_name }}</td>
                            <td>{{ $employee->email }}</td>
                            <td>{{ $employee->department->name}}</td>
                            <td>
                                <a href="{{ url('employees/'.$employee->id.'/edit') }}">
                                    <span class="btn btn-edit fa fa-pencil-square-o btn-secundary" data-placement="bottom" title="Editar"></span></a>
                                </a>
                                <a href="{{ route('employees.destroy', $employee->id) }} " onclick="return confirm('¿Desea eliminar este departamento?');">
                                    <span class="btn btn-eliminar fa fa-trash-o" data-toggle="tooltip" data-placement="bottom" title="Eliminar"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </br>
                <div align="left">
                    <i class="fa fa-bar-chart"></i>
                    <span id="products-total">{{ $employees->total() }}</span>
                    registros | página {{ $employees->currentPage() }}
                    de {{ $employees->lastPage() }}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('content_extra')
    <div align="center">
        <a href="{{ route('employees.create') }}">
            <button class="btn btn-crear">Crear</button>
        </a>
    </div>
@endsection