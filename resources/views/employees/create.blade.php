@extends ('layouts.app')

@section('content')


<div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> @lang('common.register')</h1>
    </div><!-- /.page-header -->


<div class="container">
	
	<form action="{{ url('employees') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
		
		{{ csrf_field() }}

<div class="row">
 <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.first_name')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	         <input type="text" class="form-control" name="first_name" placeholder="@lang('employee.first_name_placeholder')">
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('first_name'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('first_name') }}</strong>
	        </span>
	    </div>
	@endif
  </div>


<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.last_name')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	         <input type="text" class="form-control" name="last_name" placeholder="@lang('employee.last_name_placeholder')">
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('last_name'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('last_name') }}</strong>
	        </span>
	    </div>
	@endif
    </div>
  </div>

<div class="row">
 <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.email')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	         <input type="email" class="form-control" name="email" placeholder="@lang('employee.email_placeholder')">
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('email'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('email') }}</strong>
	        </span>
	    </div>
	@endif
  </div>	

<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('hire_date') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.hire_date')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	        <input type="date" class="form-control" name="hire_date" placeholder="Contratado"></textarea>
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('hire_date'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('hire_date') }}</strong>
	        </span>
	    </div>
	@endif
  </div>	
</div>

<div class="row">
 <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.phone')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	        <input type="number" class="form-control" name="phone" placeholder="@lang('employee.phone_placeholder')">
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('phone'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('phone') }}</strong>
	        </span>
	    </div>
	@endif
  </div>	

<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('identification') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.identification')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	          <input type="text" class="form-control" name="identification" placeholder="@lang('employee.identification_placeholder')">
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('identification'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('identification') }}</strong>
	        </span>
	    </div>
	@endif
  </div>	
</div>

<div class="row">
<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('tasks') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.tasks')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	         <textarea name="tasks" rows="4" placeholder="@lang('employee.tasks_placeholder')" class="form-control"></textarea>
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('tasks'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('tasks') }}</strong>
	        </span>
	    </div>
	@endif
  </div>	

<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('skills') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.skills')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	         <textarea name="skills" rows="4" placeholder="@lang('employee.skills')" class="form-control"></textarea>
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('skills'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('skills') }}</strong>
	        </span>
	    </div>
	@endif
  </div>	
</div>


<div class="row">
<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.department')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	         <select class="form-control" name="department_id" id="department" required>
	                <option>@lang('employee.select_department')</option>
				@foreach($departments as $department)
					<option value="{{ $department->id }}">{{ $department->name }}</option>
				@endforeach
			</select>
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('department_id'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('department_id') }}</strong>
	        </span>
	    </div>
	@endif
  </div>

<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('charge_id') ? ' has-error' : '' }}">
    <label class="col-sm-3 control-label" for="name">@lang('employee.charge')</label>
       <div class="col-sm-8">
    	  <div class="input-group">
	         <select class="form-control" name="charge_id" id="charge_id" required>
	                <option>@lang('employee.select_charge')</option>
				@foreach($charges as $charge)
					<option value="{{ $charge->id }}">{{ $charge->name }}</option>
				@endforeach
			</select>
			 <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('charge_id'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('charge_id') }}</strong>
	        </span>
	    </div>
	@endif
  </div>
</div>

<div class="row">
<div class="col-sm-6 col-md-6 col-lg-6 form-group">
   <label class="col-sm-3 control-label" for="profile_picture">Foto</label>
    <div class="col-sm-8">
    	  <div class="input-group">
			<input type="file" id="profile_picture" name="profile_picture">
		 </div>
    </div>
  </div>
</div>
		

<div class="form-group">
   <input type="submit" class="btn btn-crear" value="Guardar">
		&nbsp;
	<a href="{{ route('employees.index') }}" class="btn btn-crear">Cancelar</a>
 </div>
</form>




@endsection