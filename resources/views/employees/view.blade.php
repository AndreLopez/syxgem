@extends('layouts.app')
@section('content')

	<div class="page-header">
		<h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> Detalles del Empleado</h1>
	</div><!-- /.page-header -->

	<a href="{{ url('employees') }}" class="btn btn-default">Regresar</a>

	{{--<a href="{{ route('task.destroy', $task->id) }}" class="btn btn-danger" onclick="return confirm('¿Desea eliminar esta tarea?');">Eliminar</a>--}}
	<a href="{{ url('employees/'.$employee->id.'/edit') }}" class="btn btn-danger pull-right">Eliminar</a>
	<a href="{{ url('employees/'.$employee->id.'/edit') }}" class="btn btn-primary pull-right">Editar</a>
	<br><br>

	<div class="row">
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">Perfil</div>
				<div class="panel-body" style="text-align: center">
					<img src="{{ route('image', ['folder' => $employee->code, 'image' => $employee->profile_picture]) }}" class="img-circle" alt="Foto" height="200" width="200" >
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-default">

				<div class="panel-heading"><b>Nombre:</b> {{ $employee->first_name }} {{ $employee->last_name }}</div>
				<ul class="list-group">
					<li class="list-group-item"><b>Código:</b> {{ $employee->code }}</li>
					<li class="list-group-item"><b>Correo:</b> {{ $employee->email }}</li>
					<li class="list-group-item"><b>Habilidades:</b> {{ $employee->skills }}</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<td>Tarea</td>
						<td>Descripción</td>
						<td>Proyecto</td>
					</tr>
				</thead>
                @foreach($tasks as $task)
                    <tr>
						<td><a href="{{ url('task/'.$task->id) }}">{{ $task->name }}</a></td>
                        <td>{{ $task->description }}</td>
						<td><a href="{{ url('project/'.$task->project->id) }}">{{ $task->project->name }}</a></td>
                    </tr>
                @endforeach
			</table>
		</div>
	</div>

@section('content_extra')

@endsection

@endsection