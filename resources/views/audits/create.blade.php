@extends ('layouts.app')

@section('content')
<div class="container">

  {!! Form::open(['route' => 'audits.store', 'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

    {{ csrf_field() }}

    <div class="form-group {{ $errors->has('audit_name') ? 'has-error' : '' }}">
      <label for="audit_name">Nombre de auditoria</label>
      <input name="audit_name" type="text" class="form-control" id="audit_name" value="{{ old('audit_name') }}">
      @if ($errors->has('audit_name'))
          <span class="help-block">
              <strong>{{ $errors->first('audit_name') }}</strong>
          </span>
      @endif
      
    </div>

    <div class="form-group {{ $errors->has('comments') ? 'has-error' : '' }}">
      <label for="comments">Comentario</label>
      <input name="comments" type="text" class="form-control" id="comments" value="{{ old('comments') }}">
      @if ($errors->has('comments'))
      <span class="help-block">
        <strong>{{ $errors->first('comments') }}</strong>
      </span>
      @endif
    </div>

    <div class="form-group {{ $errors->has('reports_id') ? 'has-error' : '' }}">
      <label for="reports_id">Reports_id</label>
      <input name="reports_id" type="text" class="form-control" id="reports_id" value="{{ old('reports_id') }}">
      @if ($errors->has('reports_id'))
          <span class="help-block">
              <strong>{{ $errors->first('reports_id') }}</strong>
          </span>
      @endif
    </div>

    <div class="form-group {{ $errors->has('departaments_id') ? 'has-error' : '' }}">
      <label for="departaments_id">departaments_id</label>
        <select name="departaments_id" id="departaments_id" class="form-control">
            @foreach($departaments as $departament)
                <option value="{{ $departament->id }}">{{ $departament->name }}</option>
            @endforeach
        </select>
      @if ($errors->has('departaments_id'))
          <span class="help-block">
              <strong>{{ $errors->first('departaments_id') }}</strong>
          </span>
      @endif
    </div>

    <div class="form-group {{ $errors->has('proyects_id') ? 'has-error' : '' }}">
      <label for="proyects_id">proyects_id</label>
        <select name="proyects_id" id="proyects_id" class="form-control">
            @foreach($projects as $project)
                <option value="{{ $project->id }}">{{ $project->name }}</option>
            @endforeach
        </select>
      @if ($errors->has('proyects_id'))
          <span class="help-block">
              <strong>{{ $errors->first('proyects_id') }}</strong>
          </span>
      @endif
    </div>

    <input type="file" name="file">

    <button type="submit" class="btn btn-default">Agregar</button>

{!! Form::close() !!}
</div>

@endsection
