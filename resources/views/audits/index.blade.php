@extends ('layouts.app')

@section('content')
<div style="width:100%">

  <div style="width:100%;" class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-4 text-center">
      <br>
      <h3>Crear Nueva Auditoria:</h3>
      <br>
      <a href="{{ url('audits/create') }}" class="btn btn-default">Crear auditoria</a>
    </div>
    <div class="col-md-4">
    </div>
  </div>
  <div>
    @if(Session::has('flash_message'))
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
      </div>
    @endif
  </div>

  <table id="tabla" style="width:100%;" class="table table-hover table-striped table-responsive">
    <thead>
      <tr align="center">
        <th>id</th>
        <th>audit_name</th>
        <th>comments</th>
        <th>pathfile</th>
        <th>reports_id</th>
        <th>departaments_id</th>
        <th>proyects_id</th>
        <th>created_at</th>
        <th>updated_at</th>
      </tr>
    </thead>
  @foreach ($audits as $audit)
    <tr>
      <td>{{ $audit->id }}</td>
      <td>{{ $audit->audit_name }}</td>
      <td>{{ $audit->comments }}</td>
      <td>{{ $audit->pathfile }}</td>
      <td>{{ $audit->reports_id }}</td>
      <td>{{ $audit->departaments_id }}</td>
      <td>{{ $audit->proyects_id }}</td>
      <td>{{ $audit->created_at }}</td>
      <td>{{ $audit->updated_at }}</td>
    </tr>
    @endforeach
  </table>
  <div class="container text-center">
    <h2> Solo para gerentes de proyectos </h2>
    <br>
    <h3> Proyectos </h3>
    <form action="/audits" method="post" enctype="multipart/form-data">

        {{ csrf_field() }}

        <input type="hidden" name="proyects_id" value="1">
        <div style="width:100%; display:flex;" class=" {{ $errors->has('name_proyect') ? 'has-error' : '' }}">
          <div style="flex:1">
            <label for="name_proyect">Nombre del Proyecto</label>
            <input disabled name="name_proyect" type="text" class="form-control" id="name_proyect" value="Proyecto de empresa random">
          </div>
          <div style="flex:1">
            <label for="audit_name">Nombre de auditoria</label>
            <input name="audit_name" type="text" class="form-control" id="audit_name" value="{{ old('audit_name') }}">
            @if ($errors->has('audit_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('audit_name') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="form-group {{ $errors->has('comments') ? 'has-error' : '' }}">
          <label for="comments">Comentario</label>
          <input name="comments" type="text" class="form-control" id="comments" value="{{ old('comments') }}">
          @if ($errors->has('comments'))
          <span class="help-block">
            <strong>{{ $errors->first('comments') }}</strong>
          </span>
          @endif
        </div>

        <input type="file" name="file" >

        <button type="submit" class="btn btn-default">Agregar</button>

    </form>
    <hr>

    <h3> Departamentos </h3>
    <form action="/audits" method="post" enctype="multipart/form-data">

        {{ csrf_field() }}

        <input type="hidden" name="departaments_id" value="1">
        <div style="width:100%; display:flex;" class=" {{ $errors->has('name_proyect') ? 'has-error' : '' }}">
          <div style="flex:1">
            <label for="name_departament">Nombre del Departamento</label>
            <input disabled name="name_departament" type="text" class="form-control" id="name_proyect" value="Departamento de empresa random">
          </div>
          <div style="flex:1">
            <label for="audit_name">Nombre de auditoria</label>
            <input name="audit_name" type="text" class="form-control" id="audit_name" value="{{ old('audit_name') }}">
            @if ($errors->has('audit_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('audit_name') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="form-group {{ $errors->has('comments') ? 'has-error' : '' }}">
          <label for="comments">Comentario</label>
          <input name="comments" type="text" class="form-control" id="comments" value="{{ old('comments') }}">
          @if ($errors->has('comments'))
          <span class="help-block">
            <strong>{{ $errors->first('comments') }}</strong>
          </span>
          @endif
        </div>

        <input type="file" name="file" >

        <button type="submit" class="btn btn-default">Agregar</button>


    </form>
    <hr>

    <h3> Reportes </h3>
    <form action="/audits" method="post" enctype="multipart/form-data">

        {{ csrf_field() }}

        <input type="hidden" name="reports_id" value="1">
        <div style="width:100%; display:flex;" class=" {{ $errors->has('name_proyect') ? 'has-error' : '' }}">
          <div style="flex:1">
            <label for="name_report">Nombre del Reporte</label>
            <input disabled name="name_report" type="text" class="form-control" id="name_report" value="Reporte de empresa random">
          </div>
          <div style="flex:1">
            <label for="audit_name">Nombre de auditoria</label>
            <input name="audit_name" type="text" class="form-control" id="audit_name" value="{{ old('audit_name') }}">
            @if ($errors->has('audit_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('audit_name') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="form-group {{ $errors->has('comments') ? 'has-error' : '' }}">
          <label for="comments">Comentario</label>
          <input name="comments" type="text" class="form-control" id="comments" value="{{ old('comments') }}">
          @if ($errors->has('comments'))
          <span class="help-block">
            <strong>{{ $errors->first('comments') }}</strong>
          </span>
          @endif
        </div>

        <input type="file" name="file" >

        <button type="submit" class="btn btn-default">Agregar</button>

    </form>
    <hr>
  </div>

</div>
@endsection
