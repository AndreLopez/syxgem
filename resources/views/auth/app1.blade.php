<!DOCTYPE html>
<html>
<head>
	@include('layouts.header')
</head>
<body>
	@include('layouts.navbar')
	@yield('contenido')
	@include('layouts.footer')
</body>
</html>