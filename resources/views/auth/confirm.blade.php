@extends('layouts.app')

@section('title')
    Confirmar registro
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">@lang('auth.confirm_registration')</div>
                    <div class="panel-body">
                        <h4>{{ session('token') }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection