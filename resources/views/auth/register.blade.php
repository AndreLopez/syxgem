@extends('layouts.app')

@section('title')
    @lang('auth.register')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('auth.register')</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-7">
                            <h4 style="text-align: center">
                                <strong>@lang('auth.register_with_email')</strong>
                            </h4>
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">
                                @lang('auth.name')
                            </label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">
                                @lang('auth.email')
                            </label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">
                                @lang('auth.password')
                            </label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                                <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">
                                @lang('auth.confirm_password')
                            </label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                                <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('auth.register')
                                </button>
                            </div>
                        </div>
                            </form>
                        </div>
                        <div class="col-md-5">
                            <h4 style="text-align: center">
                                <strong>@lang('auth.register_with_social')</strong>
                            </h4>
                            <a class="btn btn-block btn-social btn-facebook"
                               href="{{route('provider', 'facebook')}}">
                                <span class="fa fa-facebook"></span>
                                @lang('auth.register_with_facebook')
                            </a>
                            <a class="btn btn-block btn-social btn-twitter"
                               href="{{route('provider', 'twitter')}}">
                                <span class="fa fa-twitter"></span>
                                @lang('auth.register_with_twitter')
                            </a>
                            <a class="btn btn-block btn-social btn-google"
                               href="{{route('provider', 'google')}}">
                                <span class="fa fa-google"></span>
                                @lang('auth.register_with_google')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
