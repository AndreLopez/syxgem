@section('content')

<div class="form-group {{ $errors->has('cliente_name)') ? 'has-error' : '' }}">
{!!Form::text('tasks', null, ['class'=>'form-control', 'id' => 'tasks', 'placeholder' => trans('status.name')]) !!}
  @if ($errors->has('tasks'))
                <span class="help-block">
                    <strong>{{ $errors->first('tasks') }}</strong>
                </span>
            @endif
		</div>

@endsection		

