<h3>@lang('email.confirm.hello') {{ $user->name }},</h3>
<p>@lang('email.confirm.clic_to_confirm'):</p>
<p><a href="{{route('confirmation', $user->confirmation_token)}}">
        {{ url('/confirmation/'.$user->confirmation_token) }}
</a></p>
<p>@lang('email.confirm.note')</p>