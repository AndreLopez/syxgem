@extends('layouts.app')
@section('content')

<div class="row affix-row">
    <div class="col-sm-3 col-md-2 affix-sidebar">
    <div class="sidebar-nav">
  <div class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <span class="visible-xs navbar-brand">Sidebar menu</span>
    </div>
    <div class="navbar-collapse collapse sidebar-navbar-collapse">
      <ul class="nav navbar-nav" id="sidenav01">
        <li class="active">
          <a href="#" data-toggle="collapse" data-target="#toggleDemo0" data-parent="#sidenav01" class="collapsed">
          <h4>
          Syxgem
          <br>
          </h4>
          </a>

        </li>
        <li>
          <a href="#" data-toggle="collapse" data-target="#toggleDemo" data-parent="#sidenav01" class="collapsed">
          <span class="glyphicon glyphicon-cloud"></span> Proyectos<span class="caret pull-right"></span>
          </a>
          <div class="collapse" id="toggleDemo" style="height: 0px;">
            <ul class="nav nav-list">
              <li><a href="#">Mis Proyectos</a></li>
              <li><a href="#">Mis Tareas</a></li>
              <li><a href="#">Mi Calendario</a></li>
            </ul>
          </div>
        </li>
          <li><a href="{{ route('department.index')}}"><span class="glyphicon glyphicon-lock"></span> Departamentos</a></li>
        <li><a href="{{ route('charge.index')}}"><span class="glyphicon glyphicon-calendar"></span> Cargos <span class="badge pull-right">42</span></a></li>

        <li><a href="#"><span class="glyphicon glyphicon-inbox"></span> Documentos <span class="caret pull-right">42</span></a></li>

         <li class="active">
          <a href="#" data-toggle="collapse" data-target="#toggleDemo2" data-parent="#sidenav01" class="collapsed">
          <span class="glyphicon glyphicon-new-window"></span> Chat <span class="caret pull-right"></span>
          </a>
          <div class="collapse" id="toggleDemo2" style="height: 0px;">
            <ul class="nav nav-list">
              <li><a href="#">Individual</a></li>
              <li><a href="#">Grupal</a></li>
            </ul>
          </div>
        </li>

      
         <li class="active">
          <a href="#" data-toggle="collapse" data-target="#toggleDemo3" data-parent="#sidenav01" class="collapsed">
          <span class="glyphicon glyphicon-cog"></span> Preferencias <span class="caret pull-right"></span>
          </a>
          <div class="collapse" id="toggleDemo3" style="height: 0px;">
            <ul class="nav nav-list">
              <li><a href="#">Idioma</a></li>
              <li><a href="#">Perfil</a></li>
            </ul>
          </div>
        </li>

  

       
      </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
  </div>
 

@endsection