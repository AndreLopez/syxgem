<!DOCTYPE html>
<html>
<head>
	@include('layouts.header')
</head>
<body>
	@include('layouts.navbar')
	@yield('content')
	@include('layouts.footer')
</body>
</html>