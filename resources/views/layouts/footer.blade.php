<footer class="footer">
        <div class="footer-inner">
            <div class="footer-content">
                <span class="bigger-120">
                    <span class="purple bolder">SyxGem</span>
                    Application © 2017
                </span>

                &nbsp; &nbsp;
                <span class="action-buttons">
                    <a href="#">
                        <span>
                            <img src="{{ asset('assets/images/ICON-FOLLOW5.png') }}" alt="" height="30">
                        </span>
                    </a>

                    <a href="#">
                        <img src="{{ asset('assets/images/ICON-FOLLOW4.png') }}" alt="" height="30">   
                    </a>

                    <a href="#">
                        <img src="{{ asset('assets/images/ICON-FOLLOW3.png') }}" alt="" height="30">
                    </a>
                </span>
            </div>
        </div>
    </footer>

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<script src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/syx.min.js') }}"></script>
{{--Chosen--}}
<script src="{{ asset('assets/js/chosen/chosen.jquery.js') }}"></script>
{{--Datepicker--}}
<script src="{{ asset('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>

<script>
    $(document).ready(function() {
        $(".chosen-select").chosen();
        $(".chosen-select").trigger('chosen:updated');

        // datepicker
        $('.input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'yyyy/mm/dd'
        });
    });

</script>