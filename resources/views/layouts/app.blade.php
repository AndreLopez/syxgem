<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.header')
</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default          ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>
        <div class="pull-right spacing-der">
            <div class="dropdown">
                <button class="btn dropdown-toggle log" type="button" data-toggle="dropdown"> {{ Auth::user()->name }}
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('account') }}">Mi Cuenta</a></li>
                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            @lang('auth.logout')
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
                @foreach(App\Models\Employee::all() as $employee)
                    @foreach(App\Models\User::all() as $user)
                        @if($employee->first_name == $user->name)
                <img src="{{ route('image', ['folder' => $employee->code, 'image' => $employee->profile_picture]) }}" height="45" class="img-circle">
               </div>
                @endif
              @endforeach
            @endforeach
            </div>
    </div><!-- /.navbar-container -->
</div>
<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
    </script>

    @include('layouts.navbar')

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">@yield('breadcrumb')</div>

            <div class="page-content">
                @yield('content')
            </div>
        </div>
        @yield('content_extra')
    </div><!-- /.main-content -->

    @include('layouts.footer')
</div>
</body>
</html>
