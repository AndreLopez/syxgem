<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta charset="utf-8">
<title>Dashboard - SyxGem</title>

<meta name="description" content="overview &amp; stats">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/font-awesome/4.5.0/css/font-awesome.min.css') }}">
<link rel="shortcut icon" href="{{ asset('images/syxgem-icon.ico') }}">
<!-- ace styles -->
<link rel="stylesheet" href="{{ asset('assets/css/syx.min.css') }}" class="ace-main-stylesheet" id="main-ace-style">
{{--Chosen--}}
<link rel="stylesheet" href="{{ asset('assets/css/chosen/bootstrap-chosen.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/chosen/chosen.css') }}">
{{--Datepicker--}}
<link rel="stylesheet" href="{{ asset('assets/css/datepicker/datepicker3.css') }}">

<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>