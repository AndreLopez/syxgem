<div id="sidebar" class="sidebar responsive ace-save-state" data-sidebar="true" data-sidebar-scroll="true" 
	 data-sidebar-hover="true">
	<script type="text/javascript">
		try{ace.settings.loadState('sidebar')}catch(e){}
	</script>

	<ul class="nav nav-list" style="top: 0px;">
		<li class="active li-alt">
			<a href="{{ url('/home') }}" class="lev">
				<span class="menu-text"> Syxgem </span>
				<img src="{{ asset('assets/images/ICON-SYXIMAGO.png') }}" alt="" height="35" align="right">
			</a>
			<b class="arrow"></b>
		</li>
		<li class="">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-suitcase"></i>
				<span class="menu-text">Proyectos</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="">
					<a href="{{route('project.index')}}">
						<i class="menu-icon fa fa-caret-right"></i> Proyectos
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="{{route('task.index')}}">
						<i class="menu-icon fa fa-caret-right"></i>Tareas
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="mi-calendario.html">
						<i class="menu-icon fa fa-address-card"></i> Mi Calendario
					</a>
				</li>
			</ul>
		</li>


		<li class="">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-address-card-o"></i>
				<span class="menu-text">Empleados</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">

				<li class="">
					<a href="{{ route('employees.index') }}">
						<i class="menu-icon fa fa-caret-right"></i> General
					</a>
				</li>
				<li class="">
					<a href="{{ route('employees-department.index') }}">
						<i class="menu-icon fa fa-caret-right"></i> Por Departamento
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="{{ route('employees-project.index')  }}">
						<i class="menu-icon fa fa-caret-right"></i> Por Proyecto
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>


		<li class="">

			<a href="{{ route('department.index') }}" class="level-0">
				<i class="menu-icon fa fa-building-o"></i>
				<span class="menu-text"> Departamentos </span>
			</a>
		</li>
		<li class="">
			<a href="{{ route('charge.index') }}" class="level-0">
				<i class="menu-icon fa fa-calendar-check-o"></i>
				<span class="menu-text"> Cargos </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li class="">
			<a href="{{route('documents.index')}}" class="level-0">
				<i class="menu-icon fa fa-folder"></i>
				<span class="menu-text"> Documentos </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li class="li-alt">
			<a href="#" class="dropdown-toggle level-1">
				<span class="menu-text"> Chat </span>
				<i class="menu-icon fa fa-comments-o fa-lg"></i>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="">
					<a href="chat-individual.html">
						<i class="menu-icon fa fa-caret-right"></i> Individual
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="chat-grupal.html">
						<i class="menu-icon fa fa-caret-right"></i> Grupal
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>
		<li class="li-alt">
			<a href="#" class="dropdown-toggle level-1">
				<span class="menu-text">Preferencias</span>
				<i class="menu-icon fa fa-cog fa-lg"></i>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="">
					<a href="idioma.html">
						<i class="menu-icon fa fa-caret-right"></i> Idioma
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="{{ route('user.index') }}">
						<i class="menu-icon fa fa-caret-right"></i> Perfil
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>
	</ul><!-- /.nav-list -->
</div>