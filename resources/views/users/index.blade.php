@extends('layouts.app')
@section('content')

<div class="page-header">
    <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45">Perfil</h1>
</div><!-- /.page-header -->


<div class="row">

    <div class="col-xs-12 bord-table">
        <div class="table-responsive">

            <table class="table table-hover">
                <thead class="text-primary doc">
                <tr>
                    <th class="first-th">Nombre</th>
                    <th>Email</th>
                    <th>Creado</th>
                    <th class="ultim-th">Acción</th>
                </tr>
                </thead>
                <tbody align="center">
                <tr class="tr">
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>
                        <a href="#">
                            <span class="btn btn-edit fa fa-pencil-square-o btn-secundary" data-placement="bottom" title="Editar"></span>
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
            </br>
        </div>
    </div>
</div>

@endsection
