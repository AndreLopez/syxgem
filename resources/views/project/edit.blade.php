@extends('layouts.app')
@section('content')

    {!! Form::open(['route'=>['project.update', $project->id], 'method'=>'PUT'])!!}
    {{ csrf_field() }}

    <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> @lang('common.edit')</h1>
    </div><!-- /.page-header -->



    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">@lang('projects.project_name')</label>
            <div class="col-sm-8">
                <div class="input-group">
                    {!!Form::text('name', $project->name, ['class'=>'form-control', 'id' => 'name', 'placeholder' => trans('projects.project_name_placeholder')]) !!}
                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('name'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                </div>
            @endif
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('client_name') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">@lang('projects.client_name')</label>
            <div class="col-sm-8">
                <div class="input-group">
                    {!!Form::text('client_name', $project->client_name, ['class'=>'form-control', 'id' => 'client_name', 'placeholder' => trans('projects.client_name_placeholder')]) !!}
                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('client_name'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('client_name') }}</strong>
            </span>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
            <label class="col-sm-3 control-label">Fecha proyecto</label>
            <div class="col-sm-8">
                <div class="input-daterange input-group" id="datepicker">
                    {!!Form::text('initiated_date', $project->initiated_date, ['class' => 'input-sm form-control', 'id' => 'initiated_date', 'placeholder' => 'Desde']) !!}
                    <span class="input-group-addon">-</span>
                    {!!Form::text('finished_date', $project->finished_date, ['class' => 'input-sm form-control', 'id' => 'finished_date', 'placeholder' => 'Hasta']) !!}

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('estimated_time') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">@lang('projects.estimated_time')</label>
            <div class="col-sm-8">
                <div class="input-group">
                    {!!Form::text('estimated_time', $project->estimated_time, ['class'=>'form-control', 'id' => 'estimated_time', 'placeholder' => trans('projects.estimated_time_placeholder')]) !!}
                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('estimated_time'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('estimated_time') }}</strong>
            </span>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">@lang('projects.department')</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <select data-placeholder="Seleccione un Departamento" class="form-control" name="department_id" id="department_id" tabindex="-1" style="" required>
                        <option>@lang('projects.select_department')</option>
                        @foreach($departments as $department)
                            <option value="{{ $department->id }}" {{ $department->id == $project->department_id ? 'selected' : '' }}>{{ $department->name }}</option>
                        @endforeach
                    </select>

                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('department_id'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('department_id') }}</strong>
            </span>
                </div>
            @endif
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">Empleados</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <select data-placeholder="Seleccione un Empleado" class="chosen-select" name="employee_id[]" id="employee_id" tabindex="-1" style="" multiple required>
                        @php($c = 0)
                        @foreach($employees as $employee)
                            @if(is_array($project_employee))
                                <option value="{{ $employee->id }}" {{ $employee->id == $project_employee[$c]->employee_id ? 'selected' : '' }}>{{ $employee->first_name }} {{ $employee->last_name }}</option>
                            @else
                                <option value="{{ $employee->id }}" {{ $employee->id == $project_employee[0]->employee_id ? 'selected' : '' }}>{{ $employee->first_name }} {{ $employee->last_name }}</option>
                            @endif
                        @php($c++)
                        @endforeach
                    </select>
                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('employee_id'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('employee_id') }}</strong>
            </span>
                </div>
            @endif
        </div>

    </div>
    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('wiki') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">@lang('projects.wiki')</label>
            <div class="col-sm-8">
                <div class="input-group">
                    {!!Form::textarea('wiki', $project->wiki, ['class'=>'form-control', 'id' => 'wiki', 'rows'=>'3',  'placeholder' => trans('projects.wiki_placeholder')]) !!}
                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('estimated_time'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('estimated_time') }}</strong>
            </span>
                </div>
            @endif
        </div>
    </div>
    <div class="form-group">
        {!!Form::submit('Guardar', ['class'=>'btn btn-crear'])!!}
        <a href="{{ route('project.index') }}" class="btn btn-crear">Cancelar</a>
    </div>

@section('content_extra')

@endsection

{!! Form::close() !!}

@endsection

