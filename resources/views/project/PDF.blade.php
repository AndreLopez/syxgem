<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proyecto {{ $project->code }}</title>

    <style>
        <?php include(public_path().'/assets/css/bootstrap.min.css');?>
        <?php /*include(public_path().'/assets/css/syx.min.css');*/?>
    </style>
</head>
<body style="background-color: white">

{{--Logo--}}
<div >
    <img src="http://localhost/sysgem/public/images/LOGO-SYXGEM-FINAL.png" style="width: 250px">
</div>

{{--Header--}}
<div class="">
    <table class="table">
        <tr>
            <td><h2>Reporte de Proyecto</h2></td>
            <td align="right"><h2>{{ $project->code }}</h2></td>
        </tr>
    </table>
</div>

{{--Contenido del proyecto--}}
<div class="panel panel-default">
    <div class="panel-heading"><b>Datos Generales</b></div>

    <table class="table">
        <tr>
            <td>Nombre del proyecto</td>
            <td>{{ $project->name }}</td>
        </tr>
        <tr>
            <td>Nombre del cliente</td>
            <td>{{ $project->client_name }}</td>
        </tr>
        <tr>
            <td>Departamento</td>
            <td>{{ $project->department->name }}</td>
        </tr>
        <tr>
            <td>Tiempo Estimado</td>
            <td>{{ $project->estimated_time }}</td>
        </tr>
        <tr>
            <td>Fecha de Inicio</td>
            <td>{{ $project->initiated_date }}</td>
        </tr>
        <tr>
            <td>Fecha de Finalización</td>
            <td>{{ $project->finished_date }}</td>
        </tr>
    </table>
    <div class="panel-footer"><b>Wiki:</b> {{ $project->wiki }}</div>
</div>
<br>

{{--Miembros del proyecto--}}
<div class="panel panel-default">
    <div class="panel-heading"><strong>Miembros del proyecto</strong></div>

    <table class="table">
        <tbody>
        @foreach($employees as $employee)
            @foreach($project_employee as $item)
                @if($employee->id == $item->employee_id)
                    <tr>
                        <td>{{ $employee->first_name }}</td>
                        <td>{{ $employee->last_name }}</td>
                    </tr>
                @endif
            @endforeach

        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>