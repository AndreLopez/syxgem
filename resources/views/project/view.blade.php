@extends('layouts.app')
@section('content')



    <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> @lang('projects.project_details')</h1>
    </div><!-- /.page-header -->

    <div class="row">
        <div class="col-md-6 bord-table">
            {{--Contenido del proyecto--}}
            <div class="panel panel-default">
                <div class="panel-heading"><b class="first-th">Datos Generales</b></div>

                <table class="table">
                    <tr>
                        <td>Código del proyecto</td>
                        <td>{{ $project->code }}</td>
                    </tr>
                    <tr>
                        <td>Nombre del proyecto</td>
                        <td>{{ $project->name }}</td>
                    </tr>
                    <tr>
                        <td>Nombre del cliente</td>
                        <td>{{ $project->client_name }}</td>
                    </tr>
                    <tr>
                        <td>Departamento</td>
                        <td>{{ $project->department->name }}</td>
                    </tr>
                    <tr>
                        <td>Tiempo Estimado</td>
                        <td>{{ $project->estimated_time }}</td>
                    </tr>
                    <tr>
                        <td>Fecha de Inicio</td>
                        <td>{{ $project->initiated_date }}</td>
                    </tr>
                    <tr>
                        <td>Fecha de Finalización</td>
                        <td>{{ $project->finished_date }}</td>
                    </tr>
                </table>
                <div class="panel-footer"><b>Wiki:</b> {{ $project->wiki }}</div>
            </div>
        </div>

        <div class="col-md-6">
            {{--Miembros del proyecto--}}
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Miembros del proyecto</strong></div>

                <table class="table">
                    <tbody>
                    @foreach($employees as $employee)
                        @foreach($project_employee as $item)
                            @if($employee->id == $item->employee_id)
                                <tr>
                                    <td><a href="{{ url('employees/'.$employee->id) }}">{{ $employee->first_name }}</td>
                                    <td>{{ $employee->last_name }}</td>
                                </tr>
                            @endif
                        @endforeach

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-6">
            {{--Miembros del proyecto--}}
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Tareas asignadas a este proyecto</strong></div>

                <table class="table">
                    <tbody>
                    @foreach($tasks as $tasks)

                                <tr>
                                    <td><a href="{{ url('task/'.$tasks->id) }}">{{ $tasks->name }}</a></td>
                                    <td>{{ $tasks->description }}</td>
                                </tr>


                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>


    </div>

    <div class="form-group">
        <a href="{{ url('project/'.$project->id.'/edit') }}" class="btn btn-crear">Editar</a>
        <a href="" class="btn btn-crear" onclick="return confirm('¿Desea eliminar esta tarea?');">Eliminar</a>
        <a href="{{ route('project.pdf', $project->id) }}" class="btn btn-crear">Descargar</a>
    </div>


@section('content_extra')

@endsection

@endsection