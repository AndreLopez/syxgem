@extends('layouts.app')

@section('content')

	<div class="page-header">
		<h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45">@lang('projects.project')</h1>
	</div><!-- /.page-header -->

	<div class="row">

		<div class="col-xs-12 bord-table">
			<div class="table-responsive">

				<table class="table table-hover">
					<thead class="text-primary doc">
					<tr>
						<th class="first-th">Nombre</th>
						  <th>Nombre del Cliente</th>
                          <th>Fecha de Inicio</th>
                          <th>Fecha de Finalización</th>
						<th class="ultim-th">Acción</th>
					</tr>
					</thead>
					<tbody align="center">
					   @foreach(\App\Models\Project::all() as $project)
						<tr class="tr">
							<td><a href="{{ url('project/'.$project->id) }}">{{ $project->name }}</a></td>
							<td>{{ $project->client_name }}</td>
							<td>{{ $project->initiated_date }}</td>
							<td>{{ $project->finished_date}}</td>
							<td>
								<a href="{{ url('project/'.$project->id.'/edit') }}">
									<span class="btn btn-edit fa fa-pencil-square-o btn-secundary" data-placement="bottom" title="Editar"></span></a>
								</a>
								<a href="{{ route('project.destroy', $project->id) }} " onclick="return confirm('¿Desea eliminar este proyecto?');">
									<span class="btn btn-eliminar fa fa-trash-o" data-toggle="tooltip" data-placement="bottom" title="Eliminar"></span>
								</a>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				</br>
				<div align="left">
					<i class="fa fa-bar-chart"></i>
					<span id="products-total">{{ $projects->total() }}</span>
					registros | página {{ $projects->currentPage() }}
					de {{ $projects->lastPage() }}
				</div>
			</div>
		</div>
	</div>


@endsection

@section('content_extra')
	<div align="center">
		<a href="{{ route('project.create') }}">
			<button class="btn btn-crear">Crear</button>
		</a>
	</div>
@endsection