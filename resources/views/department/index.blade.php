@extends('layouts.app')
@section('content')

    <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45">@lang('department.department')</h1>
    </div><!-- /.page-header -->


    <div class="row">

        <div class="col-xs-12 bord-table">
            <div class="table-responsive">

                <table class="table table-hover">
                    <thead class="text-primary doc">
                    <tr>
                        <th class="first-th">Nombre</th>
                        <th>Descripción</th>
                        <th>Análisis</th>
                        <th>Total de Empleados</th>
                        <th class="ultim-th">Acción</th>
                    </tr>
                    </thead>
                    <tbody align="center">
                    @php($c = 0)
                    @foreach($departments as $department)
                        <tr class="tr">
                            <td>{{ $department->name }}</td>
                            <td>{{ $department->description }}</td>
                            <td>{{ $department->analysis }}</td>
                            <td>{{ $total_employees[$c] }}</td>

                            <td>
                                <a href="{{ url('department/'.$department->id.'/edit') }}"  >
                                    <span class="btn btn-edit fa fa-pencil-square-o btn-secundary" data-placement="bottom" title="Editar"></span></a>
                                <a href="{{ route('department.destroy', $department->id) }} " onclick="return confirm('¿Desea eliminar este departamento?');">
                                    <span class="btn btn-eliminar fa fa-trash-o" data-toggle="tooltip" data-placement="bottom" title="Eliminar"></span>
                                </a>
                            </td>
                        </tr>
                        @php($c++)
                    @endforeach
                    </tbody>
                </table>
                </br>
                <div align="left">
                    <i class="fa fa-bar-chart"></i>
                    <span id="products-total">{{ $departments->total() }}</span>
                    registros | página {{ $departments->currentPage() }}
                    de {{ $departments->lastPage() }}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('content_extra')
    <div align="center">
        <a href="{{ route('department.create') }}">
            <button class="btn btn-crear">Crear</button>
        </a>
    </div>
@endsection



