@extends('layouts.app')
@section('content')

    {!! Form::open(['route'=>['department.update', $department->id], 'method'=>'PUT', 'files' => true])!!}
    {{ csrf_field() }}



<div class="row">
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="name">@lang('department.name')</label>
    <div class="col-sm-8">
        <div class="input-group">

               {!! Form::text('name', $department->name, ['class'=>'form-control','placeholder'=>trans('department.name_placeholder'), 'required'])!!}

    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
        </div>
    </div>
    @if ($errors->has('name'))
        <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        </div>
    @endif
  </div>
</div>
</br>

<div class="row">
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="acronym">@lang('department.description')</label>
    <div class="col-sm-8">
        <div class="input-group">

                  {!! Form::textarea('description', $department->description, ['class'=>'form-control','placeholder'=>trans('department.description_placeholder'), 'required', 'rows' => '3'])!!}

    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
        </div>
    </div>
    @if ($errors->has('description'))
        <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        </div>
    @endif
   </div>
 </div>
</br>

<div class="row">
<div class="form-group{{ $errors->has('analysis') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="acronym">@lang('department.analysis')</label>
    <div class="col-sm-8">
        <div class="input-group">

                 {!! Form::textarea('analysis', $department->analysis, ['class'=>'form-control','placeholder'=>trans('department.analysis_placeholder'), 'required', 'rows' => '3'])!!}

    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
        </div>
    </div>
    @if ($errors->has('analysis'))
        <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('analysis') }}</strong>
            </span>
        </div>
    @endif
  </div>
 </div>
</br>

<div class="row">
<div class="form-group {{ $errors->has('picture') ? 'has-error' : '' }}">
  <label class="col-sm-1 control-label" for="text">@lang('department.picture')</label>
     <div class="col-sm-8">
        <div class="input-group">
            {!! Form::file('picture') !!}
           </div>
         </div>
            @if ($errors->has('picture'))
                <span class="help-block">
                    <strong>{{ $errors->first('picture') }}</strong>
                </span>
            @endif
        </div>
     </div>


<div class="form-group">
    {!!Form::submit('Guardar',['class'=>'btn btn-crear'])!!}
        &nbsp;
        <a href="{{ route('department.index') }}" class="btn btn-crear">Cancelar</a>
</div>






{!! Form::close() !!}

@endsection
