@extends('layouts.app')
@section('content')

    {!! Form::open(['route' => 'task.store', 'method'=>'POST']) !!}
    {{ csrf_field() }}

    <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> @lang('common.register')</h1>
    </div><!-- /.page-header -->

    <div class="row">
        {{--Task name--}}
        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">@lang('tasks.name')</label>
            <div class="col-sm-8">
                <div class="input-group">
                    {!!Form::text('name', null, ['class'=>'form-control', 'id' => 'name', 'placeholder' => trans('tasks.name_placeholder'), 'required']) !!}
                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('name'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                </div>
            @endif
        </div>

        {{--Task project--}}
        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">@lang('tasks.project')</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <select class="form-control" name="project_id" id="project_id" tabindex="-1" style="" required>
                        <option>@lang('tasks.select_project')</option>
                        @foreach($projects as $project)
                            <option value="{{ $project->id }}">{{ $project->name }}</option>
                        @endforeach
                    </select>

                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('project_id'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('project_id') }}</strong>
            </span>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        {{--Task description--}}
        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">@lang('tasks.description')</label>
            <div class="col-sm-8">
                <div class="input-group">
                    {!! Form::textarea('description', null, ['class'=>'form-control','placeholder'=>trans('tasks.description_placeholder'), 'required', 'rows' => '3'])!!}
                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('description'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
                </div>
            @endif
        </div>

        {{--Task employee--}}
        <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label" for="name">@lang('tasks.employee')</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <select class="form-control" name="employee_id" id="employee_id" tabindex="-1" style="" required>
                        <option>@lang('tasks.select_employee')</option>
                        @foreach($employees as $employee)
                            <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->last_name }}</option>
                        @endforeach
                    </select>

                    <span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
                </div>
            </div>
            @if ($errors->has('employee_id'))
                <div class="col-sm-8 col-sm-offset-3">
            <span class="help-block">
                <strong>{{ $errors->first('employee_id') }}</strong>
            </span>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        {{--Task dates--}}
        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
            <label class="col-sm-3 control-label">Tiempo estimado</label>
            <div class="col-sm-8">
                <div class="input-daterange input-group" id="datepicker">
                    {!!Form::text('start', null, ['class' => 'input-sm form-control', 'id' => 'start', 'placeholder' => 'Desde']) !!}
                    <span class="input-group-addon">-</span>
                    {!!Form::text('end', null, ['class' => 'input-sm form-control', 'id' => 'end', 'placeholder' => 'Hasta']) !!}

                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        {!!Form::submit('Guardar', ['class'=>'btn btn-crear'])!!}
        <a href="{{ route('task.index') }}" class="btn btn-crear">Cancelar</a>
    </div>



@section('content_extra')

@endsection

{!! Form::close() !!}

@endsection