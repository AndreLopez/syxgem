@extends('layouts.app')
@section('content')



    <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> @lang('tasks.detail')</h1>
    </div><!-- /.page-header -->

    <div class="panel panel-default bord-table">
        <!-- Default panel contents -->
        <div class="panel-heading"><th class="first-th">@lang('projects.name'):</th> {{ $task->name }}</div>
        <div class="panel-body">
            <b>@lang('tasks.description'):</b>
            <p>{{ $task->description }}</p>
        </div>

        <!-- List group -->
        <ul class="list-group">
            <li class="list-group-item"><b>@lang('tasks.project'):</b><a href="{{ url('project/'.$task->project->id) }}"> {{ $task->project->name }}</a></li>
            <li class="list-group-item"><b>@lang('tasks.employee'):</b><a href="{{ url('employees/'.$task->employee->id) }}"> {{ $task->employee->first_name }} {{ $task->employee->last_name }}</a></li>
            <li class="list-group-item"><b>@lang('tasks.star_date'):</b> {{ $task->start }}</li>
            <li class="list-group-item"><b>@lang('tasks.end_date'):</b> {{ $task->end }}</li>
        </ul>
    </div>


    <div class="form-group">
        <a href="{{ url('task/'.$task->id.'/edit') }}" class="btn btn-crear">Editar</a>
        <a href="{{ route('task.destroy', $task->id) }}" class="btn btn-crear" onclick="return confirm('¿Desea eliminar esta tarea?');">Eliminar</a>
        <a href="{{ url('/task') }}" class="btn btn-crear">Volver</a>
    </div>

@section('content_extra')

@endsection

@endsection