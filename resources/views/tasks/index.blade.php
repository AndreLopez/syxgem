@extends('layouts.app')

@section('content')

    <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45">@lang('tasks.tasks')</h1>
    </div><!-- /.page-header -->

    <div class="row">

        <div class="col-xs-12 bord-table">
            <div class="table-responsive">

                <table class="table table-hover">
                    <thead class="text-primary doc">
                    <tr>
                        <th class="first-th">Nombre</th>
                        <th>Descripción</th>
                        <th>Proyecto</th>
                        <th>Empleado</th>
                        <th class="ultim-th">Acción</th>
                    </tr>
                    </thead>
                    <tbody align="center">
                    @foreach(\App\Models\Task::all() as $task)
                        <tr class="tr">
                            <td><a href="{{ url('task/'.$task->id) }}">{{ $task->name }}</a></td>
                            <td>{{ $task->description }}</td>
                            <td>{{ $task->project->name }}</td>
                            <td>{{ $task->employee->first_name }} {{ $task->employee->last_name }}</td>
                            <td>
                                <a href="{{ url('task/'.$task->id.'/edit') }}">
                                    <span class="btn btn-edit fa fa-pencil-square-o btn-secundary" data-placement="bottom" title="Editar"></span></a>
                                </a>
                                <a href="{{ route('task.destroy', $task->id) }} " onclick="return confirm('¿Desea eliminar esta tarea?');">
                                    <span class="btn btn-eliminar fa fa-trash-o" data-toggle="tooltip" data-placement="bottom" title="Eliminar"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </br>
                <div align="left">
                    <i class="fa fa-bar-chart"></i>
                    <span id="products-total">{{ $tasks->total() }}</span>
                    registros | página {{ $tasks->currentPage() }}
                    de {{ $tasks->lastPage() }}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('content_extra')
    <div align="center">
        <a href="{{ route('task.create') }}">
            <button class="btn btn-crear">Crear</button>
        </a>
    </div>
@endsection