<div class="modal fade" tabindex="-1" role="dialog" id="modal-dropout">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('auth.dropout')</h4>
            </div>
            <div class="modal-body">
                <h4 style="text-align: center">@lang('auth.sure_to_delete')</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">@lang('auth.not_delete')</button>
                <form action="{{route('drop_account')}}" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger">
                        @lang('auth.yes_delete_account')
                    </button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal" id="pleaseWait" data-backdrop="static">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-time"></span>
                    @lang('messages.wait')
                </h4>
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-info active" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>