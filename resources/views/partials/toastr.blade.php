<script>
    $(document).ready(function () {
        toastr.options = {
            "progressBar": true
        };
        @if(session('message'))
            @if(session('message')['alert'] == 'success')
                toastr.success("{{session('message')['text']}}");
            @elseif(session('message')['alert'] == 'danger')
                toastr.error("{{session('message')['text']}}");
            @endif
            {{session()->forget('message')}}
        @endif
    });
</script>