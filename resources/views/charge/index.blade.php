@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> Cargos</h1>
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12 bord-table">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="text-primary doc">
                    <tr>
                        <th class="first-th">Nombre</th>
                        <th>Descripción</th>
                        <th>Análisis</th>

                        <th class="ultim-th">Acción</th>
                    </tr>
                    </thead>
                    <tbody align="center">
                    @foreach($charges as $charge)
                        <tr class="tr">
                            <td>{{ $charge->name }}</td>
                            <td>{{ $charge->description }}</td>
                            <td>{{ $charge->analysis }}</td>

                            <td>
                                <a href="{{ url('charge/'.$charge->id.'/edit') }}"  >
                                    <span class="btn btn-edit fa fa-pencil-square-o btn-secundary" data-placement="bottom" title="Editar"></span></a>
                                <a href="{{ route('charge.destroy', $charge->id) }} " onclick="return confirm('¿Desea eliminar este cargo?');">
                                    <span class="btn btn-eliminar fa fa-trash-o" data-toggle="tooltip" data-placement="bottom" title="Eliminar"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </br>
                <div align="left">
                    <i class="fa fa-bar-chart"></i>
                    <span id="products-total">{{ $charges->total() }}</span>
                    registros | página {{ $charges->currentPage() }}
                    de {{ $charges->lastPage() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content_extra')
    <div align="center">
        <a href="{{ route('charge.create') }}">
            <button class="btn btn-crear">Crear</button>
    </div>
@endsection

