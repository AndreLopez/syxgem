@extends('layouts.app')
@section('content')

{!! Form::open(['route'=>['charge.update', $charge->id], 'method'=>'PUT'])!!}
{{ csrf_field() }}


<div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> @lang('charges.charges_register')</h1>
    </div><!-- /.page-header -->


<div class="row">
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	<label class="col-sm-1 control-label" for="name">@lang('charges.name')</label>
    <div class="col-sm-8">
    	<div class="input-group">

			   {!! Form::text('name', $charge->name, ['class'=>'form-control','placeholder'=>trans('charges.name_placeholder'), 'required'])!!}
 	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('name'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('name') }}</strong>
	        </span>
	    </div>
	@endif
  </div>
</div>
</br>

<div class="row">
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
	<label class="col-sm-1 control-label" for="acronym">@lang('charges.description')</label>
    <div class="col-sm-8">
    	<div class="input-group">

			     {!! Form::textarea('description', $charge->description, ['class'=>'form-control','placeholder'=>trans('charges.description_placeholder'), 'required', 'rows' => '3'])!!}


 	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('description'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('description') }}</strong>
	        </span>
	    </div>
	@endif
   </div>
 </div>
</br>

<div class="row">
<div class="form-group{{ $errors->has('analysis') ? ' has-error' : '' }}">
	<label class="col-sm-1 control-label" for="acronym">@lang('charges.analysis')</label>
    <div class="col-sm-8">
    	<div class="input-group">

			     {!! Form::textarea('analysis', $charge->analysis, ['class'=>'form-control','placeholder'=>trans('charges.analysis_placeholder'), 'required', 'rows' => '3'])!!}


 	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('analysis'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('analysis') }}</strong>
	        </span>
	    </div>
	@endif
  </div>
</div>


<div class="form-group">
	{!!Form::submit('Guardar',['class'=>'btn btn-crear'])!!}
		&nbsp;
		<a href="{{ route('charge.index') }}" class="btn btn-crear">Cancelar</a>

</div>



{!! Form::close() !!}

@endsection
