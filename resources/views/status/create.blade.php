@extends ('layouts.app')

@section('content')


{!! Form::open(['route'=>'status.store', 'method'=>'POST'])!!}
{{ csrf_field() }}



<div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> @lang('status.register_status')</h1>
    </div><!-- /.page-header -->




<div class="row">
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	<label class="col-sm-1 control-label" for="name">@lang('status.name')</label>
    <div class="col-sm-8">
    	<div class="input-group">

			   {!! Form::text('name', null, ['class'=>'form-control','placeholder'=>trans('status.name'), 'required'])!!}
 	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('name'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('name') }}</strong>
	        </span>
	    </div>
	@endif
  </div>
</div>



<div class="form-group">
	{!!Form::submit('Guardar',['class'=>'btn btn-crear'])!!}
		&nbsp;
		<a href="{{ route('status.index') }}" class="btn btn-crear">Cancelar</a>
</div>






{!! Form::close() !!}

@endsection


