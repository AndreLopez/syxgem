@extends('layouts.app')
@section('content')

  <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45">@lang('status.status')</h1>
    </div><!-- /.page-header -->


 <div class="row">

        <div class="col-xs-12 bord-table">
            <div class="table-responsive">
                
                <table class="table table-hover">
                    <thead class="text-primary doc">
                        <tr>
                            <th class="first-th">Nombre</th>
                            <th class="ultim-th">Acción</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                       
                          @foreach($status as $status)
                        <tr class="tr">
                            <td>{{ $status->name }}</td>
                            
                            <td>
                        <a href="{{ url('status/'.$status->id.'/edit') }}"  >
                                <span class="btn btn-edit fa fa-pencil-square-o btn-secundary" data-placement="bottom" title="Editar"></span></a>
                                 <a href="{{ route('status.destroy', $status->id) }} " onclick="return confirm('¿Desea eliminar este departamento?');">
                                <span class="btn btn-eliminar fa fa-trash-o" data-toggle="tooltip" data-placement="bottom" title="Eliminar"></span>
                              </a>
                            </td>
                        </tr>
                         
                     @endforeach
                    </tbody>
                </table>
            
        </div>
    </div>
    
    
@endsection

@section('content_extra')
    <div align="center">
    <a href="{{ route('status.create') }}">
        <button class="btn btn-crear">Crear</button>
    </div>
@endsection

