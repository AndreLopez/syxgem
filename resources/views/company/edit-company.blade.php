@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('auth.register')</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-7">
                           
                            <form class="form-horizontal" role="form" method="PUT" action="{{ route ('company.update')}}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">
                                @lang('company.name')
                            </label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $company->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                                <div class="form-group{{ $errors->has('company_info') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">
                                @lang('company.company_info')
                            </label>

                            <div class="col-md-6">
                                <input id="company_info" type="text" class="form-control" name="company_info" value="{{$company->company_info}}" required>

                                @if ($errors->has('company_info'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_info') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                                <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('company.edit')
                                </button>
                            </div>
                        </div>
                            </form>
                        </div>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>









    
@endsection