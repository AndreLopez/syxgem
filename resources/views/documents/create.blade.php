@extends ('layouts.app')

@section('content')
<div class="container">
  {!! Form::open(['route' => 'documents.store', 'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

    {{ csrf_field() }}

    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
      <label for="name">Nombre de Documento</label>
      <input name="name" type="text" class="form-control" id="name" value="{{ old('name') }}">
      @if ($errors->has('name'))
          <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
          </span>
      @endif

    </div>

    <div class="form-group {{ $errors->has('comments') ? 'has-error' : '' }}">
      <label for="comments">Comentario</label>
      <input name="comments" type="text" class="form-control" id="comments" value="{{ old('comments') }}">
      @if ($errors->has('comments'))
      <span class="help-block">
        <strong>{{ $errors->first('comments') }}</strong>
      </span>
      @endif
    </div>

    <div class="form-group {{ $errors->has('code') ? 'has-error' : '' }}">
      <label for="code">code</label>
      <input name="code" type="text" class="form-control" id="code" value="{{ old('code') }}">
      @if ($errors->has('code'))
          <span class="help-block">
              <strong>{{ $errors->first('code') }}</strong>
          </span>
      @endif
    </div>

    <div class="form-group {{ $errors->has('documents_details') ? 'has-error' : '' }}">
      <label for="documents_details">documents_details</label>
      <input name="documents_details" type="text" class="form-control" id="documents_details" value="{{ old('documents_details') }}">
      @if ($errors->has('documents_details'))
          <span class="help-block">
              <strong>{{ $errors->first('documents_details') }}</strong>
          </span>
      @endif
    </div>

    <div class="form-group {{ $errors->has('reports_id') ? 'has-error' : '' }}">
      <label for="reports_id">reports_id</label>
      <input name="reports_id" type="number" class="form-control" id="reports_id" value="{{ old('reports_id') }}">
      @if ($errors->has('reports_id'))
          <span class="help-block">
              <strong>{{ $errors->first('reports_id') }}</strong>
          </span>
      @endif
    </div>

    <div class="form-group {{ $errors->has('audits_id') ? 'has-error' : '' }}">
      <label for="audits_id">audits_id</label>
        <select name="audits_id" id="audits_id" class="form-control" >
            @foreach($audits as $audit)
                <option value="{{ $audit->id }}">{{ $audit->audit_name }}</option>
            @endforeach
        </select>

      @if ($errors->has('audits_id'))
          <span class="help-block">
              <strong>{{ $errors->first('audits_id') }}</strong>
          </span>
      @endif
    </div>

    <div class="checkbox">
      <label><input type="checkbox" name="restricted" value="true">Restringir modificación</label>
    </div>

    <input type="file" name="file">

    <button type="submit" class="btn btn-default">Agregar</button>

    {!! Form::close() !!}
</div>

@endsection
