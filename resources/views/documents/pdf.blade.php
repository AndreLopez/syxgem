<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Constacia</title>
    <style media="screen">
      table{
        margin-bottom: 20px;
      }
    </style>
  </head>
  <body>
    <small style="float:left">{{ Carbon\Carbon::now() }}</small>
    <small style="float:right">Codigo:9876543210</small>
    <div style="text-align:center">
      <h1>Constancia de trabajo</h1>
    </div>
    <br>
    <br>
    <br>
    <strong>Datos de empleado</strong>
    <table id="tabla" style="width:100%;">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Email</th>
          <th>Telefono</th>
          <th>Fecha de ingreso</th>
        </tr>
      </thead>
      <tr>
        <td>Jose</td>
        <td>Castillo</td>
        <td>jose@mail.com</td>
        <td>0415777227</td>
        <td>04/04/2017</td>
      </tr>
    </table>

    <strong>Cargo de empleado</strong>
    <table id="tabla" style="width:100%;">
      <thead>
        <tr>
          <th>Cargo</th>
          <th>Descripcion</th>
        </tr>
      </thead>
      <tr>
        <td>Analista</td>
        <td>Breve descripcion de las tares del empleado</td>
      </tr>
    </table>

    <strong>Participacion en proyectos</strong>
    <table id="tabla" style="width:100%;">
      <thead>
        <tr>
          <th>Proyecto</th>
          <th>Dia de inicio</th>
          <th>Dia final</th>
          <th>detall del proyecto</th>
        </tr>
      </thead>
      <tr>
        <td>Syxgem</td>
        <td>04/04/2017</td>
        <td>Actual</td>
        <td>Systema de gestion de tareas</td>
      </tr>
      <tr>
        <td>Licencias Keys</td>
        <td>04/10/2017</td>
        <td>04/15/2017</td>
        <td>E-commerce hecha con wordpress</td>
      </tr>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div style="display:inline-block;width:30%;margin:auto">
      <hr>
    </div>
    <br>
    <div style="text-align:center">
      <strong>Firma</strong>
    </div>
  </body>
</html>
