@extends ('layouts.app')

@section('content')
<div style="width:100%">

  <div style="width:100%;" class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-4 text-center">
      <br>
      <h3>Crear Nuevo Documento:</h3>
      <br>
      <a href="{{ url('documents/create') }}" class="btn btn-default">Crear Documento</a>
    </div>
    <div class="col-md-4">
    </div>
  </div>
  <div>
    @if(Session::has('flash_message'))
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
      </div>
    @endif
    @if(Session::has('flash_alert'))
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_alert') }}
      </div>
    @endif
  </div>

  <table id="tabla" style="width:100%;" class="table table-hover table-striped table-responsive">
    <thead>
      <tr align="center">
        <th>id</th>
        <th>name</th>
        <th>code</th>
        <th>comments</th>
        <th>documents_details</th>
        <th>drive</th>
        <th>restricted</th>
        <th>audits_id</th>
        <th>reports_id</th>
        <th>created_at</th>
        <th>updated_at</th>
        <th colspan="2">acciones</th>
      </tr>
    </thead>
  @foreach ($documents as $document)
    <tr>
      <td>{{ $document->id }}</td>
      <td>{{ $document->name }}</td>
      <td>{{ $document->code }}</td>
      <td>{{ $document->comments }}</td>
      <td>{{ $document->documents_details }}</td>
      <td>{{ $document->drive }}</td>
      @if ($document->restricted == 1)
        <td>true</td>
      @else
        <td>false</td>
      @endif
      <td>{{ $document->audits_id }}</td>
      <td>{{ $document->reports_id }}</td>
      <td>{{ $document->created_at }}</td>
      <td>{{ $document->updated_at }}</td>
      <td><a href="documents/{{ $document->id }}/edit">Modificar</a></td>
      <td>
        <form action="documents/{{ $document->id }}" method="post">
          {!! method_field('delete') !!}
          {{ csrf_field() }}
          <button type="submit">Borrar</button></td>
        </form>
    </tr>
    @endforeach
  </table>

  <br>
  <br>
  <div class="container text-center">
    <h3>Editor de documentos</h3>
    <textarea></textarea>
  </div>

</div>
@endsection
