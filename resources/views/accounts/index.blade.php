@extends('layouts.app')


@section('modals')
    @include('partials.modals')
@endsection

@section('content')


    <div class="page-header">
        <h1><img src="assets/images/ICON-ACTIVIDADES.png" height="45"> @lang('auth.account')</h1>
    </div><!-- /.page-header -->
    <div class="container bord-table">
                 <div class="row">
                     <div class="col-md-12" style="text-align: center">
                         <div class="thumbnail">
                             @foreach(App\Models\Employee::all() as $employee)
                                 @foreach(App\Models\User::all() as $user)
                                     @if($employee->first_name == $user->name)
                             <img src="{{ route('image', ['folder' => $employee->code, 'image' => $employee->profile_picture]) }}" class="img-circle" width="250" height="250">
                             @endif
                            @endforeach
                          @endforeach
                                 <div class="caption">
                                 <h1>{{Auth::user()->name}}</h1>
                                        <form class="form-horizontal" role="form" method="POST" action="{{ route('update_account') }}">
                                            {{ csrf_field() }}
                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <label class="col-sm-3 control-label" for="name">@lang('auth.name')</label>
                                                <div class="col-md-6">
                                                    <input id="name" type="text"
                                                           class="form-control" name="name"
                                                           value="{{ old('name') ? old('name'): Auth::user()->name}}" required autofocus>

                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label class="col-sm-3 control-label" for="name"> @lang('auth.email')</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="email"
                                                           class="form-control" name="email"
                                                           value="{{ old('email') ? old('email'): Auth::user()->email}}" required>

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label class="col-sm-3 control-label" for="name">@lang('auth.password')</label>
                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control" name="password">

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div  class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                <label class="col-sm-3 control-label" for="name">   @lang('auth.confirm_password')</label>

                                                <div class="col-md-6">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                    <label class="col-sm-3 control-label" for="profile_picture">Foto</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <input type="file" id="profile_picture" name="profile_picture">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-crear" >Actualizar</button>
                                                &nbsp;
                                                <a href="{{ url('home') }}" class="btn btn-crear">Cancelar</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


        </div>





@endsection

@section('scripts')
    @include('partials.toastr')
    <script>
        $(document).ready(function(){
            $('a.dropout').on('click', function(eventLink){
                eventLink.preventDefault();
                $("#modal-dropout").modal("show");
            });
        });
    </script>
@endsection