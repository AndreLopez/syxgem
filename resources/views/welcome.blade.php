<!DOCTYPE html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SyxGem</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="FREEHTML5.CO" />


  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="images/syxgem-icon.ico">
	<link rel="stylesheet" href="css/style-video-full-screen.css">
	<link href="https://fonts.googleapis.com/css?family=Karla:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	
	<!-- Flexslider  -->
	<link rel="stylesheet" href="css/flexslider.css">

	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/estilos.css">
	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>


	</head>
	<body>

	<nav id="fh5co-main-nav" role="navigation">
		<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle active"><i></i></a>
		<div class="js-fullheight fh5co-table">
			<div class="fh5co-table-cell js-fullheight">
				<ul>
					<li><a href="index.html" class="option-menu">Servicios</a></li>
					<li><a href="#" class="option-menu">Recursos</a></li>
					<li><a href="#" class="option-menu">Chat & Video</a></li>
					<li><a href="#" class="option-menu">Apps</a></li>
					<li><a href="#" class="option-menu">Soporte</a></li>
				</ul>
				<p class="fh5co-social-icon">
					<a href="#"><i class="icon-twitter2"></i></a>
					<a href="#"><i class="icon-facebook2"></i></a>
					<a href="#"><i class="icon-instagram"></i></a>
					<a href="#"><i class="icon-dribbble2"></i></a>
					<a href="#"><i class="icon-youtube"></i></a>
				</p>
			</div>
		</div>
	</nav>
	
	<div id="fh5co-page">
<header>
			<div class="container">
				<div class="row fh5co-navbar-brand">
					<div class="col-xs-12 col-sm-4">
						<h1 class="text-align">
					 	<a href="index.html">
							<img src="images/LOGO-SYXGEM-FINAL.png" alt="" class="icon-logo-syxgem">
						</a>
					</h1>
					</div>
				<div class="col-sm-8 animate-box text-center">
						<ul class="options-menu">
							<li><a href="#" class="option-menu">Servicios</a></li>
							<li><a href="#" class="option-menu">Recursos</a></li>
							<li><a href="#" class="option-menu">Chat & Video</a></li>
							<li><a href="#" class="option-menu">Apps</a></li>
							<li><a href="#" class="option-menu">Soporte</a></li>
						</ul>
					</div> 
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle margin-top"><i></i></a>
				</div>
			</div>
		</header>
	<!--<div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-2 animate-box text-center">
						<ul class="options-menu">
							<li>Servicios</li>
							<li>Recursos</li>
							<li>Chat & Video</li>
							<li>Apps</li>
							<li>Soporte</li>
						</ul>
					</div>
				</div>
			</div>
		</div> -->
	

		<aside id="fh5co-hero" class="js-fullheight">
			<div class="flexslider js-fullheight" align="center">
				<ul class="slides animate-box">
			   		<li style="background-image: url(images/FONDO-FOOTER1.jpg);" class="fondo-section-1">
			   			<div class="overlay-gradient"></div>	
			   		</li>
			  	</ul>
		  	</div>
		</aside>

		<section>
			<div class="container-fluid animate-box">
				<div class="row">
					<div class="col-xs-8 fondo-left">
						<p class="titles-1">Sistema de Gestión Empresarial</p>
					</div>
					<div class="col-xs-4 fondo-right" align="center">
						<a href="{{ url('/login') }}"><p class="titles-2">Regístrate o <br>Inicia Sesión</p></a>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="container animate-box">
				<div class="row cont-text-img">
					<div class="col-xs-12 col-sm-6">
						<h1 class="title-section-3">Trabaja desde, donde y cuando quieras</h1>
						<p class="text-section-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi dolorem quidem mollitia, incidunt laborum et dignissimos maiores eveniet impedit autem quas ullam, reiciendis neque, ratione. Distinctio id, blanditiis earum possimus!</p>
					</div>
					<div class="col-xs-12 col-sm-6">
						<img src="images/ICON-SYXGEM.png" class="img-responsive img-devices">
					</div>
				</div>
			</div>
		</section>

		<section id="fh5co-hero" class="js-fullheight">
			<div class="flexslider js-fullheight" align="center">
				<ul class="slides animate-box">
			   		<li style="background-image: url(images/FONDO-HOME1.jpg);">
			   			<div class="overlay-gradient">
			   			</div>
			   			<div class="row">
			   				<div class="col-xs-12 col-sm-6 col-sm-push-5 cont-tlqn">		
			   					<h1 class="title-need">Todo lo que necesitas, en un sólo lugar</h1>	
			   					<button class="btn btn-aprende">Aprende más</button>
			   				</div>
			   			</div>
			   		</li>
			  	</ul>
		  	</div>
		</section>

		<section>
			<div class="container-fluid cont">
				<div class="row" align="center">
					<div class="col-xs-12">
						<h1 class="title-pqs">¿Por qué SYXGEM?</h1>
					</div>
				<div class="row" align="center">
					<div class="col-xs-12 col-sm-4 animate-box">
						<div class="row">
							<img src="images/ICON-HOME1.png">
						</div>
						<div class="row">
							<h3>Gestión segura de usuario</h3>
							<p class="texts">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quaerat dolor optio perspiciatis similique nostrum repudiandae laboriosam illum autem totam. Necessitatibus, quam corporis nostrum esse voluptates fugit est at pariatur.</p>
						</div>	
					</div>
					<div class="col-xs-12 col-sm-4 animate-box">
						<div class="row">
							<img src="images/ICON-HOME2.png">
						</div>
						<div class="row">
							<h3>Asignación y entrega de archivos</h3>
							<p class="texts">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quaerat dolor optio perspiciatis similique nostrum repudiandae laboriosam illum autem totam. Necessitatibus, quam corporis nostrum esse voluptates fugit est at pariatur.</p>
						</div>	
					</div>
					<div class="col-xs-12 col-sm-4 animate-box">
						<div class="row">
							<img src="images/ICON-HOME3.png">
						</div>
						<div class="row">
							<h3>Chats y grupos de trabajo</h3>
							<p class="texts">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quaerat dolor optio perspiciatis similique nostrum repudiandae laboriosam illum autem totam. Necessitatibus, quam corporis nostrum esse voluptates fugit est at pariatur.</p>
						</div>	
					</div>
				</div>
				<div class="row" align="center">
					<div class="col-xs-12 col-sm-4 animate-box">
						<div class="row">
							<img src="images/ICON-HOME4.png">
						</div>
						<div class="row">
							<h3>Gestión a tu medida</h3>
							<p class="texts">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quaerat dolor optio perspiciatis similique nostrum repudiandae laboriosam illum autem totam. Necessitatibus, quam corporis nostrum esse voluptates fugit est at pariatur.</p>
						</div>	
					</div>
					<div class="col-xs-12 col-sm-4 animate-box">
						<div class="row">
							<img src="images/ICON-HOME5.png">
						</div>
						<div class="row">
							<h3>Sincronización con dispositivos</h3>
							<p class="texts">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quaerat dolor optio perspiciatis similique nostrum repudiandae laboriosam illum autem totam. Necessitatibus, quam corporis nostrum esse voluptates fugit est at pariatur.</p>
						</div>	
					</div>
					<div class="col-xs-12 col-sm-4 animate-box">
						<div class="row">
							<img src="images/ICON-HOME6.png">
						</div>
						<div class="row">
							<h3>Video conferencias</h3>
							<p class="texts">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quaerat dolor optio perspiciatis similique nostrum repudiandae laboriosam illum autem totam. Necessitatibus, quam corporis nostrum esse voluptates fugit est at pariatur.</p>
						</div>	
					</div>
				</div>
				</div>
			</div>
		</section>

		<div class="row padding-row animate-box">
			<div class="col-xs-12">
				<div class="col-sm-1 hidden-xs cont-ext-purple">
					<div class="col-xs-10 hidden-xs cont-int-purple-izq">
						
					</div>
				</div>
				<div class="col-sm-10 cont-central">
					
				</div>
				<div class="col-sm-1 hidden-xs cont-ext-purple">
					<div class="col-xs-10 hidden-xs cont-int-purple-der">
						
					</div>
				</div>
			</div>
		</div>

		<section>
			<div class="container animate-box" style="height:677px;">
				<div class="row">
					<div class="col-xs-12">
						
  <video poster="images/img-intro.png" id="bgvid" playsinline muted loop>

<!-- <source src="http://thenewcode.com/assets/videos/polina.webm" type="video/webm"> -->
<source src="video/video-negocios.mp4" type="video/mp4">
</video>

<div id="polina">
	<img src="images/ICON-HOME7.png" class="img-responsive ">
	<h2 class="text-video" id="text-1">Mira nuestro Video</h2>
	<button><img src="images/ICON-PLAYBUTTON.png" class="btn-play"></button> 
	<h2 class="text-video" id="text-2">Y vive una experiencia<br> SYXGEM</h2>
</div>

					</div>
				</div>
			</div>
		</section>

<section>
		<div class="slides animate-box">
			<div class="cont-gestor">
			   	<div class="overlay-gradient"></div>
			   	<div class="row">
			   		<div class="col-xs-12 col-sm-6 col-md-5">		
			   			<img src="images/ICON-FORMULARIOHOME.png" class="img-responsive">		
			   		</div>
			   		<div class="col-xs-12 col-sm-6 col-md-7 padding-form">
						<h2 class="title-gest-empr">Tu empresa no tiene gestor empresarial?</h2>
						<p class="escribenos">¡Escríbenos! Te brindaremos la mejor asesoría.</p>
						<form>
							<div class="form-group">
								<div class="row">
									<div class="col-xs-12 col-sm-6">
										<label class="label-home">Nombre:</label>
										<input class="form-control input-home" type="text">	
									</div>
									<div class="col-xs-12 col-sm-6">
										<label class="label-home">Empresa:</label>
										<input class="form-control input-home" type="text">	
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<label class="label-home">Correo:</label>
										<input class="form-control input-home" type="email">	
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<label class="label-home">Mensaje:</label>
										<textarea name=""  class="form-control input-home" rows="4"></textarea>
									</div>
								</div>
									<span class="btn btn-enviar">Enviar</span>
								<p class="com">Nos comunicaremos con usted, personalmente.</p>
							</div>							
						</form>
			   		</div>
			   	</div>
			</div>
		</div>
</section>

	<!--	<div id="fh5co-services-section">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center animate-box ">
						<div class="heading-section">
							<h2>Our Services</h2>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<div class="icon">
								<span><i class="icon-mobile"></i></span>
							</div>
							<h3>Responsive</h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<div class="icon">
								<span><i class="icon-browser"></i></span>
							</div>
							<h3>Compatible to all browser</h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<div class="icon">
								<span><i class="icon-toolbox"></i></span>
							</div>
							<h3>Web Design</h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="fh5co-work-section">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center animate-box ">
						<div class="heading-section">
							<h2>Our Website</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-center animate-box">
						<div class="work" style="background-image: url(images/img-1.jpg);">
							<a href="#" class="view">
								<span>View Demo</span>
							</a>
						</div>
					</div>
					<div class="col-md-6 text-center animate-box">
						<div class="work" style="background-image: url(images/img-2.jpg);">
							<a href="#" class="view">
								<span>View Demo</span>
							</a>
						</div>
					</div>
					<div class="col-md-6 text-center animate-box">
						<div class="work" style="background-image: url(images/img-3.jpg);">
							<a href="#" class="view">
								<span>View Demo</span>
							</a>
						</div>
					</div>
					<div class="col-md-6 text-center animate-box">
						<div class="work" style="background-image: url(images/img-4.jpg);">
							<a href="#" class="view">
								<span>View Demo</span>
							</a>
						</div>
					</div>
					<p class="text-center view-button animate-box"><a href="#" class="btn btn-primary btn-outline btn-lg">See More Project</a></p>
				</div>
			</div>
		</div> 

		<div id="fh5co-work-section">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center animate-box">
						<div class="heading-section">
							<h2>Branding</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 text-center animate-box">
						<div class="work" style="background-image: url(images/img-5.jpg);">
							<a href="#" class="view">
								<span>View Demo</span>
							</a>
						</div>
					</div>
					<div class="col-md-8 text-center animate-box">
						<div class="work" style="background-image: url(images/img-6.jpg);">
							<a href="#" class="view">
								<span>View Demo</span>
							</a>
						</div>
					</div>
					<div class="col-md-7 text-center animate-box">
						<div class="work" style="background-image: url(images/img-7.jpg);">
							<a href="#" class="view">
								<span>View Demo</span>
							</a>
						</div>
					</div>
					<div class="col-md-5 text-center animate-box">
						<div class="work" style="background-image: url(images/img-8.jpg);">
							<a href="#" class="view">
								<span>View Demo</span>
							</a>
						</div>
					</div>
					<p class="text-center view-button animate-box"><a href="#" class="btn btn-primary btn-outline btn-lg">See More branding</a></p>
				</div>
			</div>
		</div>

		<div id="fh5co-product-section">
			<div class="container">
				<div class="row">
					<div class="col-md-4 prod text-center animate-box">
						<div class="product" style="background-image: url(images/img-9.jpg);">
							<a href="#" class="view">
								<i class="icon-plus"></i>
							</a>
						</div>
						<h3><a href="#">Bag</a></h3>
						<span class="price">$48</span>
					</div>
					<div class="col-md-4 prod text-center animate-box">
						<div class="product" style="background-image: url(images/img-11.jpg);">
							<a href="#" class="view">
								<i class="icon-plus"></i>
							</a>
						</div>
						<h3><a href="#">Arigato Shoes</a></h3>
						<span class="price">$109</span>
					</div>
					<div class="col-md-4 prod text-center animate-box">
						<div class="product" style="background-image: url(images/img-12.jpg);">
							<a href="#" class="view">
								<i class="icon-plus"></i>
							</a>
						</div>
						<h3><a href="#">New Balance Shoes</a></h3>
						<span class="price">$89</span>
					</div>
					<p class="text-center view-button animate-box"><a href="#" class="btn btn-primary btn-outline btn-lg">See More Product</a></p>
				</div>
			</div>
		</div> 

		<div class="fh5co-counters" style="background-image: url(images/hero.jpg);" data-stellar-background-ratio="0.5" id="counter-animate">
			<div class="fh5co-narrow-content animate-box">
				<div class="row" >
					<div class="col-md-4 text-center">
						<span class="fh5co-counter js-counter" data-from="0" data-to="130" data-speed="5000" data-refresh-interval="50"></span>
						<span class="fh5co-counter-label">Website</span>
					</div>
					<div class="col-md-4 text-center">
						<span class="fh5co-counter js-counter" data-from="0" data-to="1450" data-speed="5000" data-refresh-interval="50"></span>
						<span class="fh5co-counter-label">Branding</span>
					</div>
					<div class="col-md-4 text-center">
						<span class="fh5co-counter js-counter" data-from="0" data-to="7497" data-speed="5000" data-refresh-interval="50"></span>
						<span class="fh5co-counter-label">Product</span>
					</div>
				</div>
			</div>
		</div> -->

		<footer class="animate-box">
			<div id="footer">
				<div class="container">
					<div class="row cont-text-footer" align="center">

						<div class="col-xs-12 col-sm-3 col-md-2">
							<h3 class="section-title">Servicios</h3>
							<ul class="items-footer">
								<li><a href="#" style="color: white">Planes</a></li>
								<li><a href="#" style="color: white">Precios</a></li>
								<li><a href="#" style="color: white">Plataformas</a></li>
								<li><a href="#" style="color: white">Newsletter</a></li>
								<li><a href="#" style="color: white">Integraciones</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-2">
							<h3 class="section-title">Recursos</h3>
							<ul class="items-footer">
								<li><a href="#" style="color: white">Blog</a></li>
								<li><a href="#" style="color: white">Tutoriales</a></li>
								<li><a href="#" style="color: white">Webinar</a></li>
							</ul>
						</div>

						<div class="col-xs-12 col-sm-3 col-md-4">
							<h3 class="section-title">Chat &amp; Video</h3>
							<ul class="items-footer">
								<li><a href="#" style="color: white">Video-Conferencias</a></li>
								<li><a href="#" style="color: white">Chat Room</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-2" style="text-align: center;">
							<h3 class="section-title">Apps</h3>
							<ul class="items-footer">
								<li><a href="#" style="color: white">Movil</a></li>
								<li><a href="#" style="color: white">Escritorio</a></li>
								<li><a href="#" style="color: white">Interfaz</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 space-bottom">
							<h3 class="section-title">Soporte</h3>
							<ul class="items-footer">
								<li><a href="#" style="color: white">Contáctenos</a></li>
								<li><a href="#" style="color: white">Soporte en Vivo</a></li>
								<li><a href="#" style="color: white">Recursos Humanos</a></li>
							</ul>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4">
            			<div id="imaginary_container"> 
                			<div class="input-group stylish-input-group">
                    			<input type="text" class="form-control placeh-buscar" placeholder="Buscar..." >
                    			<span class="input-group-addon">
                        			<button type="submit">
                            			<span class="glyphicon glyphicon-search"></span>
                        			</button>  
                    			</span>
                			</div>
            			</div>
        			</div>
						<div class="col-xs-12 col-sm-8 col-red-social">
							<p class="text-sig">Síguenos</p>
							<div class="row">
								<div class="col-xs-12 cont-redes">
									<a href="#" target="_blank">
										<img src="images/ICON-FOLLOW5.png" alt="icon-facebook" class="icon-red-social"> 
									</a>
									<a href="#" target="_blank">
										<img src="images/ICON-FOLLOW4.png" alt="icon-twitter" class="icon-red-social">
									</a>
									<a href="#" target="_blank">
										<img src="images/ICON-FOLLOW3.png" alt="icon-instagram" class="icon-red-social">
									</a>
									<a href="#" target="_blank">
										<img src="images/ICON-FOLLOW2.png" alt="icon-google+" class="icon-red-social">
									</a>
									<a href="#" target="_blank">
										<img src="images/ICON-FOLLOW1.png" alt="icon-youtub" class="icon-red-social">
									</a>
								</div>
							</div>
						</div>
				</div>
			</div>
		</footer>
	
	</div>

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Counters -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>

<!--	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->

    <script src="js/index.js"></script>

	<script src="js/log-reg.js"></script>
	<!-- Main JS -->
	<script src="js/main.js"></script>
<script type="text/javascript" charset="utf-8" async defer>
				//Funcion de Cambio de estilo de titulo de Inicio de Sesion y Crear Cuenta
						$(document).ready(function(){
							$("#login-form-link").addClass("activo");
						    $("#register-form-link").removeClass("activo");
						    $("#sub-log").addClass("activo");
				//Función de Cambio Inicio de Sesión --> Crear Cuenta
    						$("#login-form-link").click(function(){
						    $("#login-form-link").addClass("activo");
						    $("#register-form-link").removeClass("activo");
						    $("#sub-log").addClass("activo");
						    $("#sub-reg").removeClass("activo");
						});
    			//Función de Cambio Inicio de Sesión <-- Crear Cuenta
						$("#register-form-link").click(function(){
						   $("#login-form-link").removeClass("activo");
						  $("#register-form-link").addClass("activo");
						   $("#sub-log").removeClass("activo");
						  $("#sub-reg").addClass("activo");
						});   
						});

					</script>
	</body>
</html>

