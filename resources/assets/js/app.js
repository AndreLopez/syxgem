
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 Vue.component('example', require('./components/Example.vue'));
 Vue.component('chat-message', require('./components/ChatMessage.vue'));
 Vue.component('chat-log', require('./components/ChatLog.vue'));
 Vue.component('chat-composer', require('./components/ChatComposer.vue'));

const app = new Vue({
    el: '#app',
    data: {
      messages: [],
      usersInRoom: []
    },
    methods: {
      addMessage(message) {
          this.messages.push(message);
          console.log("message added");
          axios.post('/messages', message).then(response => {
            // console.log(response);
          })
          .catch(function (error) {
                if (error.response) {
                  // The request was made, but the server responded with a status code
                  // that falls out of the range of 2xx
                  console.log(error.response.data);
                  console.log(error.response.status);
                  console.log(error.response.headers);
                } else {
                  // Something happened in setting up the request that triggered an Error
                  console.log('Error', error.message);
                }
                console.log(error.config);
          });
      }
    },
    created(){
      axios.get('/messages').then(response =>{
        this.messages = response.data;
        console.log(response);
      });

      Echo.join('chatroom')
          .here((users) => {
            this.usersInRoom = users;
          })
          .joining((user) => {
            this.usersInRoom.push(user);
          })
          .leaving((user) => {
            this.usersInRoom = this.usersInRoom.filter(u => u != user)
          })
          .listen('MessagePosted', (e) => {
            console.log(e);
            this.messages.push({
              message: e.message.message,
              user: e.user
            });
      });
    }
});
