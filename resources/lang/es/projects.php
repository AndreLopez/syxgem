<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
   'project' => 'Proyectos',
   'project_name' => 'Nombre del Proyecto', 
   'project_name_placeholder' => 'Inserte nombre del proyecto',   
   'name' => 'Nombre', 
   'initiated_date' => 'Fecha de inicio', 
    'finished_date' => 'Fecha de finalización', 
    'client_name' => 'Nombre del Cliente',
    'client_name_placeholder' => 'Inserte nombre del cliente', 
    'tasks' => 'Tareas',
    'wiki' => 'Descripción',
    'wiki_placeholder' => ' Ingrese descripción del proyecto',
    'details_tasks' => 'Detalles de las tareas asignadas', 
    'sub_tasks' => 'Sub Tareas',
    'estimated_time' => 'Tiempo Estimado', 
    'estimated_time_placeholder' => 'Tiempo estimado para la realización del proyecto', 
    'goals' => 'Metas', 
    'wiki' => 'Wiki', 
    'comments' => 'Comentarios',  

    'department' => 'Departamento', 
    'select_department' => 'Seleccionar Departamento', 

    'employees_on_this_project' => 'Añadir Empleados al Proyecto', 
    'add_employee' => 'Añadir Empleado',

    'project_details' => 'Detalles del proyecto',


];
