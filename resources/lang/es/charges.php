<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name' => 'Nombre',
    'name_placeholder' => 'Inserte Nombre',  
    'description' => 'Descripción del cargo',
    'description_placeholder' => 'Inserte Descripción', 
    'analysis' => 'Análisis del Cargo', 
    'analysis_placeholder' => 'Inserte Análisis del Cargo',  
    'edit' => 'Editar', 
    'charges_register' => 'Registro de Cargos', 
    'edit_charges' => 'Editar',

    
    
]; 
