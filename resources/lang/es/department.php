<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Department Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'department' => 'Departamento',

    'name' => 'Nombre',
    'description'     => 'Descripción',
    'analysis' => 'Análisis', 
    'total_employees' => 'Total de empleados',
    'picture' => 'Imagen del Departamento',

    'name_placeholder' => 'Ingrese nombre', 
    'description_placeholder' => 'Ingrese descripción', 
    'analysis_placeholder' => 'Ingrese análisis del department', 
    'total_employees_placeholder' => 'Ingrese Total de Empleados'

];
