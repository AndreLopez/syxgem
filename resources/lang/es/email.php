<?php

return [
    'confirm' => [
        'name' => 'Confirmación Sysgem',
        'subject' => 'Correo de confirmación',
        'hello' => 'Hola',
        'clic_to_confirm' => 'Has clic en el siguiente enlace para confirmar registro',
        'note' => 'Nota: Si no puedes acceder, copia y pega el enlace en tu navegador.',
    ],

    'reset_passwd' => [
        'name' => 'Sysgem - Reseteo',
        'subject' => 'Reinicio de contraseña',
        'greeting' => 'Hola',
        'receive_request' => 'Te enviamos éste correo porque recibimos una solicitud de reseteo de contraseña para su cuenta.',
        'action' => 'Restablecer contraseña',
        'delete_email' => 'Si usted no realizó ninguna solicitud, simplemente elimine éste correo.',
        'regards' => 'Saludos',
        'trouble' => 'Si tienes problemas al hacer clic en el botón ":button", copia y pega la URL a continuación en tu navegador web:'
    ],
];