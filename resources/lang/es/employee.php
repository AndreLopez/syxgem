<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Employees Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'first_name' => 'Nombre',
    'first_name_placeholder' => ' Inserte Nombre',
    'last_name' => 'Apellido', 
    'last_name_placeholder' => 'Inserte Apellido', 
    'email' => 'Correo', 
    'email_placeholder' => 'Inserte Correo', 
    'tasks' => 'Tareas',
    'tasks_placeholder' => 'Descripción de las Tareas Asignadas a su puesto', 
    'hire_date' => 'Fecha de Contratación', 
    'phone' => 'Teléfono', 
    'phone_placeholder' => 'Inserte número de teléfono', 
    'identification' => 'Identificación', 
    'identification_placeholder' => 'Inserte Identificación', 
    'skills' => 'Habilidades', 
    'skills_plaholder' => 'Inserte habilidades', 
    'department' => 'Departamento',
    'select_department' => 'Seleccionar Departamento',  
    'charge' => 'Cargo', 
    'select_charge' => 'Seleccionar Cargo',
    'employee_details' => 'Detalles del empleado',

    
    
]; 
