<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'confirm_registration' => 'Confirma tu registro',
    'token' => 'Te hemos enviado un correo electrónico para que confirmes tu registro.',
    'invalid_token' => 'El Código es inválido o ya te encuentras registrado.',
    'confirmed' => 'Has confirmado correctamente, ya puedes iniciar sesión.',
    'verify_email' => 'Verifica tu correo y confirma el registro antes de iniciar sesión por primera vez.',
    'remember' => 'Recuerda mis datos',
    'login' => 'Inciar sesión',
    'register' => 'Registrarse',
    'logout' => 'Cerrar sesión',
    'name' => 'Nombre',
    'email' => 'Correo electrónico',
    'password' => 'Contraseña',
    'confirm_password' => 'Confirma contraseña',
    'signin_with_email' => 'Ingresa con tu e-mail',
    'signin_with_social' => 'O ingresa con tus redes sociales',
    'signin_with_twitter' => 'Ingresa con Twitter',
    'signin_with_facebook' => 'Ingresa con Facebook',
    'signin_with_google' => 'Ingresa con Google',
    'register_with_email' => 'Resgístrate con tu e-mail',
    'register_with_social' => 'O regístrate con tus redes sociales',
    'register_with_twitter' => 'Regístrate con Twitter',
    'register_with_facebook' => 'Regístrate con Facebook',
    'register_with_google' => 'Regístrate con Google',
    'account' => 'Mi cuenta',
    'update_account' => 'Actualizar mis datos',
    'delete_account' => 'Eliminar Cuenta',
    'dropout' => 'Darse de baja',
    'sure_to_delete' => '¿Seguro que deseas eliminar tu cuenta?',
    'not_delete' => 'No quiero',
    'yes_delete_account' => 'Si quiero eliminar mi cuenta',
];
