<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tasks Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'tasks'                   => 'Tareas',
    'name'                    => 'Nombre de Tarea',
    'name_placeholder'        => 'Inserte nombre de la tarea',
    'description'             => 'Descripción',
    'description_placeholder' => 'Describa la tarea',
    'project'                 => 'Proyecto',
    'select_project'          => 'Seleccionar Proyecto',
    'employee'                => 'Empleado',
    'select_employee'         => 'Seleccionar Empleado',
    'detail'                  => 'Detalles',
     'star_date'    => 'Fecha de Inicio',
    'end_date'     => 'Fecha de Cierre',

];
