<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tasks Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'tasks' => 'Tasks',
    'name' => 'Task name',
    'name_placeholder' => 'Introduce tasks name',
    'description' => 'Description',
    'description_placeholder' => 'Describe the task',
    'project' => 'Project',
    'select_project' => 'Select Project',
    'employee' => 'Employee',
    'select_employee' => 'Select Employee',

];
