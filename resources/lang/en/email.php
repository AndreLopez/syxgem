<?php

return [
    'confirm' => [
        'name' => 'Sysgem Confirmation',
        'subject' => 'Confirmation Email',
        'hello' => 'Hello',
        'clic_to_confirm' => 'Click on the following link to confirm registration',
        'note' => 'Note: If you can not access, copy and paste the link in your browser.',
    ],

    'reset_passwd' => [
        'name' => 'Sysgem - Reset',
        'subject' => 'Reset Password',
        'greeting' => 'Hello',
        'receive_request' => 'You are receiving this email because we received a password reset request for your account.',
        'action' => 'Reset Password',
        'delete_email' => 'If you did not request a password reset, no further action is required.',
        'regards' => 'Regards',
        'trouble' => 'If you’re having trouble clicking the ":button" button, copy and paste the URL below into your web browser:',
    ],
];