<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'confirm_registration' => 'Confirm your registration',
    'token' => 'We have sent you an email to confirm your registration.',
    'invalid_token' => 'The code is invalid or you are already registered.',
    'confirmed' => 'You have successfully confirmed, you can now login.',
    'verify_email' => 'Check your email and confirm the registration before you log in for the first time.',
    'remember' => 'Remember me',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'name' => 'Name',
    'email' => 'E-mail address',
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'signin_with_email' => 'Enter with your e-mail',
    'signin_with_social' => 'Or enter with your social networks',
    'signin_with_twitter' => 'Sign in with Twitter',
    'signin_with_facebook' => 'Sign in with Facebook',
    'signin_with_google' => 'Sign in with Google',
    'register_with_email' => 'Register with your e-mail',
    'register_with_social' => 'Register with your social networks',
    'register_with_twitter' => 'Register with Twitter',
    'register_with_facebook' => 'Register with Facebook',
    'register_with_google' => 'Register with Google',
    'account' => 'My account',
    'update_account' => 'Update my account',
    'delete_account' => 'Delete account',
    'dropout' => 'Drop out',
    'sure_to_delete' => 'Are you sure you want to delete your account?',
    'not_delete' => 'I do not want',
    'yes_delete_account' => 'Yes, I want to delete my account',
];
